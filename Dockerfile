# BUILDER IMAGE
FROM bitwalker/alpine-elixir-phoenix:1.6.4 as builder

ENV MIX_ENV=prod
ENV APP=hidria

# RUN apk upgrade --no-cache && \
# apk add --no-cache git openssh

COPY mix.exs mix.lock ./
COPY config config

RUN mix do deps.get, deps.compile

COPY . .
RUN mix compile \
&& (cd assets; npm install; brunch build -p) \
&& mix phx.digest \
&& mix release --env=prod
RUN (mix app.version | tail -n 1) > .version \
&& cp _build/prod/rel/$APP/releases/$(cat .version)/$APP.tar.gz .

# THE REAL IMAGE
# ==============
FROM alpine:3.7

ENV APP=hidria

RUN apk upgrade --no-cache && \
apk add --no-cache bash openssl imagemagick file

EXPOSE 4000
ENV PORT=4000 SHELL=/bin/bash

COPY --from=builder /opt/app/.version .
COPY --from=builder /opt/app/$APP.tar.gz .
RUN tar -zxf $APP.tar.gz

CMD ./bin/$APP foreground
