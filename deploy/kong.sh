#!/bin/bash

KONG_GATEWAY="127.0.0.1"

echo "removing kms service"
curl -X DELETE http://$KONG_GATEWAY:8001/apis/kms

echo "=== --- ==="
echo "registering kms service"
curl -X POST \
     --url http://$KONG_GATEWAY:8001/apis/ \
     --data "name=kms" \
     --data "upstream_url=http://kms.service:4000" \
     --data "hosts=f4w.corp.hidria.com" \
     --data "strip_uri=false"
