defmodule Seed do
  alias Hidria.Shopfloor
  alias Hidria.Defsol

  def alphabeth do
    for n <- ?a..?z, do: <<n::utf8>>
  end

  @lorem """
  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  """

  def random_name(len \\ 5) do
    alphabeth()
    |> Enum.shuffle()
    |> Enum.take(len)
    |> Enum.join()
  end

  def random(chance \\ 0.5, first, second) do
    case :rand.uniform() >= chance do
      true -> first
      _ -> second
    end
  end

  def user_fixture(term, role) do
    %{
      fullname: term,
      username: term,
      password: term,
      password_confirmation: term,
      role: role
    }
  end

  def part_fixture(term) do
    %{code: term, description: "#{term} part code description"}
  end

  def station_fixture(term) do
    %{code: term, description: "#{term} station description"}
  end

  def error_fixture(title, part_id, station_id) do
    %{title: title, description: @lorem, part_id: part_id, station_id: station_id}
  end

  def cause_fixture(title, description, detection, error_id) do
    %{title: title, description: description, detection: detection, error_id: error_id}
  end

  def parts(n \\ 12) do
    Shopfloor.delete_all_parts()

    for _i <- 1..n do
      random_name()
      |> part_fixture()
      |> Shopfloor.create_part()
    end
    |> Enum.map(fn {:ok, item} -> item end)
  end

  def stations(n \\ 12) do
    Shopfloor.delete_all_stations()

    for _i <- 1..n do
      random_name()
      |> station_fixture()
      |> Shopfloor.create_station()
    end
    |> Enum.map(fn {:ok, item} -> item end)
  end

  def errors(n \\ 12, parts, stations) do
    Defsol.delete_all_errors()

    1..n
    |> Enum.map(fn _ -> error(parts, stations) end)
  end

  def error(parts, stations) do
    part = random(0, Enum.random(parts).id, nil)
    station = random(0, Enum.random(stations).id, nil)

    {:ok, error} =
      random_name()
      |> error_fixture(part, station)
      |> Defsol.create_error()

    error
  end

  def causes(n \\ 2, error) do
    for _ <- 1..n do
      {:ok, cause} =
        random_name()
        |> cause_fixture(@lorem, @lorem, error.id)
        |> Defsol.create_cause()

      cause
    end
  end

  def solution_fixture(cause_id) do
    %{
      title: random_name(),
      description: @lorem,
      cause_id: cause_id,
      steps: [
        %{
          title: random_name(),
          description: @lorem
        },
        %{
          title: random_name(),
          description: @lorem
        }
      ]
    }
  end

  def solution(cause) do
    {:ok, solution} = solution_fixture(cause.id) |> Defsol.create_solution()

    solution
  end
end

alias Hidria.Accounts
alias Hidria.Defsol

users = [
  Seed.user_fixture("admin", "admin"),
  Seed.user_fixture("leader", "lead"),
  Seed.user_fixture("operator", "operator")
]

Accounts.delete_all_users()
users = users |> Enum.map(&Accounts.create_user(&1))

# stations = Seed.stations(1)
# parts = Seed.parts(2)
# errors = Seed.errors(3, parts, stations)

# Defsol.delete_all_causes()
# causes = errors |> Enum.map(&Seed.causes(&1)) |> List.flatten()

# Defsol.delete_all_solutions()
# solutions = causes |> Enum.map(&Seed.solution(&1))
