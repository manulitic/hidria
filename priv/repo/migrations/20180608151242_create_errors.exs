defmodule Hidria.Repo.Migrations.CreateErrors do
  use Ecto.Migration

  def change do
    create table(:errors, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:title, :string)
      add(:part_id, references(:parts, on_delete: :delete_all, type: :binary_id))
      add(:station_id, references(:stations, on_delete: :delete_all, type: :binary_id))

      timestamps()
    end

    create(index(:errors, [:part_id]))
    create(index(:errors, [:station_id]))
  end
end
