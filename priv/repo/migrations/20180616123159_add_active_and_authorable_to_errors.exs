defmodule Hidria.Repo.Migrations.AddActiveAndAuthorableToErrors do
  use Ecto.Migration

  def change do
    alter table(:errors) do
      add(:active, :boolean, default: false)
      add(:inserted_by, references(:users, on_delete: :nothing, type: :binary_id))
      add(:updated_by, references(:users, on_delete: :nothing, type: :binary_id))
    end

    create(index(:errors, [:inserted_by]))
  end
end
