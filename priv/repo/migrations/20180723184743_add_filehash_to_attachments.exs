defmodule Hidria.Repo.Migrations.AddFilehashToAttachments do
  use Ecto.Migration

  def change do
    alter table(:attachments) do
      add(:filehash, :string, null: false)
    end

    create(index(:attachments, [:filehash]))
  end
end
