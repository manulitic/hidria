defmodule Hidria.Repo.Migrations.CreateAttachments do
  use Ecto.Migration

  def change do
    create table(:attachments, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:fileinfo, :string, null: false)
      add(:error_id, references(:errors, on_delete: :delete_all, type: :binary_id))

      timestamps()
    end

    create(index(:attachments, [:error_id]))
  end
end
