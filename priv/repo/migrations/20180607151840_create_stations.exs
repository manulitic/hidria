defmodule Hidria.Repo.Migrations.CreateStations do
  use Ecto.Migration

  def change do
    create table(:stations, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:code, :string)
      add(:description, :string)

      timestamps()
    end

    create(index("stations", [:id]))
    create(index("stations", [:code], unique: true))
  end
end
