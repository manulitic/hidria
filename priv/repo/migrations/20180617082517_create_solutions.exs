defmodule Hidria.Repo.Migrations.CreateSolutions do
  use Ecto.Migration

  def change do
    create table(:solutions, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:title, :string)
      add(:description, :text)
      add(:cause_id, references(:causes, on_delete: :delete_all, type: :binary_id))
      add(:inserted_by, references(:users, on_delete: :nothing, type: :binary_id))
      add(:updated_by, references(:users, on_delete: :nothing, type: :binary_id))

      timestamps()
    end

    create(index(:solutions, [:cause_id]))
    create(index(:solutions, [:inserted_by]))
  end
end
