defmodule Hidria.Repo.Migrations.CreateParts do
  use Ecto.Migration

  def change do
    create table(:parts, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:code, :string, null: false)
      add(:description, :string)

      timestamps()
    end

    create(index("parts", [:id]))
    create(index("parts", [:code], unique: true))
  end
end
