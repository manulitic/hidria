defmodule Hidria.Repo.Migrations.AddDescriptionToErrors do
  use Ecto.Migration

  def change do
    alter table(:errors) do
      add(:description, :text)
    end
  end
end
