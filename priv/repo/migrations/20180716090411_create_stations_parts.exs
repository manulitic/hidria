defmodule Hidria.Repo.Migrations.CreateStationsParts do
  use Ecto.Migration

  def change do
    create table(:stations_parts, primary_key: false) do
      add(:station_id, references(:stations, type: :binary_id))
      add(:part_id, references(:parts, type: :binary_id))
    end
  end
end
