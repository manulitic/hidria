defmodule Hidria.Repo.Migrations.CreateNotes do
  use Ecto.Migration

  def change do
    create table(:notes, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:description, :text)
      add(:error_id, references(:errors, on_delete: :delete_all, type: :binary_id))
      add(:cause_id, references(:causes, on_delete: :delete_all, type: :binary_id))
      add(:solution_id, references(:solutions, on_delete: :delete_all, type: :binary_id))
      add(:inserted_by, references(:users, on_delete: :nothing, type: :binary_id))
      add(:updated_by, references(:users, on_delete: :nothing, type: :binary_id))

      timestamps()
    end

    create(index(:notes, [:error_id]))
    create(index(:notes, [:cause_id]))
    create(index(:notes, [:solution_id]))
    create(index(:notes, [:inserted_by]))
  end
end
