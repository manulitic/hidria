defmodule Hidria.Repo.Migrations.AddAttachmentsToSolutionStep do
  use Ecto.Migration

  def change do
    alter table(:attachments) do
      add(
        :solution_step_id,
        references(:solutions_steps, on_delete: :delete_all, type: :binary_id)
      )
    end

    create(index(:attachments, [:solution_step_id]))
  end
end
