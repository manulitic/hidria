defmodule Hidria.Repo.Migrations.AddIndexToUsersRole do
  use Ecto.Migration

  def change do
    create(index(:users, [:role]))
  end
end
