defmodule Hidria.Repo.Migrations.CreateCauses do
  use Ecto.Migration

  def change do
    create table(:causes, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:title, :string)
      add(:description, :text)
      add(:detection, :text)
      add(:active, :boolean, default: false, null: false)
      add(:error_id, references(:errors, on_delete: :delete_all, type: :binary_id))
      add(:inserted_by, references(:users, on_delete: :nothing, type: :binary_id))
      add(:updated_by, references(:users, on_delete: :nothing, type: :binary_id))

      timestamps()
    end

    create(index(:causes, [:error_id]))
    create(index(:causes, [:inserted_by]))
  end
end
