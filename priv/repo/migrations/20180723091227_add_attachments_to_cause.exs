defmodule Hidria.Repo.Migrations.AddAttachmentsToCause do
  use Ecto.Migration

  def change do
    alter table(:attachments) do
      add(:cause_id, references(:causes, on_delete: :delete_all, type: :binary_id))
    end

    create(index(:attachments, [:cause_id]))
  end
end
