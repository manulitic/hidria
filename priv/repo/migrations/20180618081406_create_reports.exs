defmodule Hidria.Repo.Migrations.CreateReports do
  use Ecto.Migration

  def change do
    create table(:reports, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:description, :text)
      add(:success, :boolean, default: true, null: false)
      add(:solution_id, references(:solutions, on_delete: :nothing, type: :binary_id))
      add(:inserted_by, references(:users, on_delete: :nothing, type: :binary_id))
      add(:updated_by, references(:users, on_delete: :nothing, type: :binary_id))

      timestamps()
    end

    create(index(:reports, [:solution_id]))
    create(index(:reports, [:inserted_by]))
  end
end
