defmodule Hidria.Repo.Migrations.CreateSolutionsSteps do
  use Ecto.Migration

  def change do
    create table(:solutions_steps, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:title, :string)
      add(:description, :text)
      add(:solution_id, references(:solutions, on_delete: :delete_all, type: :binary_id))

      timestamps()
    end

    create(index(:solutions_steps, [:solution_id]))
  end
end
