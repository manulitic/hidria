# Hidria

[![pipeline status](https://gitlab.com/manulitic/hidria/badges/master/pipeline.svg)](https://gitlab.com/manulitic/hidria/commits/master)

Knowledge Management System

## Deployment

Running the application as docker container under systemd. In order to work
properly it will need `docker` and `proxy` network.
