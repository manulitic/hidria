VERSION=`mix app.version | tail -n 1`
IMAGE=registry.gitlab.com/manulitic/hidria

.PHONY: assets docker test

assets:
	cd assets && brunch b -p
	mix phx.digest

install:
	cp deploy/kms.service /etc/systemd/system/
	cp deploy/kms.postgres.service /etc/systemd/system/
	systemctl daemon-reload

clean:
	systemctl stop kms
	systemctl stop kms.postgres
	rm /etc/systemd/system/kms.service
	rm /etc/systemd/system/kms.postgres.service
	systemctl daemon-reload

docker:
	docker build -t ${IMAGE}:${VERSION} .

docker.push:
	docker push ${IMAGE}:${VERSION}

test:
	mix format --check-formatted
	mix compile --no-deps-check --force --warnings-as-errors
	mix test
