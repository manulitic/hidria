# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :hidria, ecto_repos: [Hidria.Repo]

# Configures the endpoint
config :hidria, HidriaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bdWpL6trTB2c0ioLXtiPW9BtgsN7iYc4sHR1YEeRjvCaDoaRmQlfJrXVSVVZVMI+",
  render_errors: [view: HidriaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Hidria.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :hidria, :generators,
  migration: true,
  binary_id: true

config :scrivener_html,
  routes_helper: HidriaWeb.Router.Helpers,
  view_style: :bootstrap_v4

config :pumba,
  logo: [
    bold: "FACTS4",
    normal: "workers",
    link: "/"
  ]

config :arc,
  storage: Arc.Storage.Local

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
