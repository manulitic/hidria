// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

$(document).ready(function() {
    $('.select2-class').select2();

    $(".btn-toggle").on('click',function(){
        $(this).children('.fa-chevron-right, .fa-chevron-down').toggleClass("fa-chevron-right fa-chevron-down");
    });
});

// bootstrap popper tooltips
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
})

import "formstone/dist/js/core.js";
import "formstone/dist/js/upload.js";

var error_upload = $(".error-upload");
error_upload.upload({
    action: "/api/attachments",
    postKey: "fileinfo",
    postData: {error_id: error_upload.data("assocId")}
});
error_upload.on("complete", function(e) {
    window.location.reload();
});
error_upload.on("start", function(e) {
    document.getElementById("overlay").style.display = "block";
});

var cause_upload = $(".cause-upload");
cause_upload.upload({
    action: "/api/attachments",
    postKey: "fileinfo",
    postData: {cause_id: cause_upload.data("assocId")}
});
cause_upload.on("complete", function(e) {
    window.location.reload();
});
cause_upload.on("start", function(e) {
    document.getElementById("overlay").style.display = "block";
});

$(".solution-step-upload").each(function (index, elem) {
    var step_upload = $("#" + elem.id);
    step_upload.upload({
        action: "/api/attachments",
        postKey: "fileinfo",
        postData: {solution_step_id: step_upload.data("assocId")}
    });
    step_upload.on("complete", function(e) {
        window.location.reload();
    });
    step_upload.on("start", function(e) {
        document.getElementById("overlay").style.display = "block";
    });
});

import "formstone/dist/js/touch.js";
import "formstone/dist/js/transition.js";
import "formstone/dist/js/viewer.js";
import "formstone/dist/js/lightbox.js";
$(".lightbox").lightbox();
