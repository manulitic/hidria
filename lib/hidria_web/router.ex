defmodule HidriaWeb.Router do
  use HidriaWeb, :router

  alias HidriaWeb.Plugs.AssignSession
  alias HidriaWeb.Plugs.AuthUser
  alias HidriaWeb.Plugs.AuthEnsure
  alias HidriaWeb.Plugs.AuthRole
  alias HidriaWeb.Plugs.CheckState

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(AssignSession, [:current_station, :current_part])
    plug(AuthUser)
  end

  pipeline :authorize do
    plug(AuthEnsure)
  end

  pipeline :state do
    plug(CheckState, :station)
    plug(CheckState, :part)
  end

  pipeline :operator do
    plug(AuthEnsure)
    plug(AuthRole, :operator)
  end

  pipeline :lead do
    plug(AuthEnsure)
    plug(AuthRole, :lead)
  end

  pipeline :admin do
    plug(AuthEnsure)
    plug(AuthRole, :admin)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", HidriaWeb do
    pipe_through(:browser)
    get("/login", LoginController, :new)
    post("/login", LoginController, :create)
  end

  scope "/", HidriaWeb do
    pipe_through([:browser, :operator])
    resources("/state", StateController, only: [:show, :create, :delete], param: "resource")
    get("/logout", LoginController, :delete)

    resources("/user", UserController, only: [:show, :edit, :update])
    get("/users/:id/newpassword", UserController, :new_password)
    put("/users/:id/updatepassword", UserController, :update_password)

    get("/attachments", Defsol.AttachmentController, :index)
    delete("/attachments/:id", Defsol.AttachmentController, :delete)
  end

  scope "/", HidriaWeb do
    pipe_through([:browser, :operator, :state])

    get("/", HomeController, :index)
  end

  scope "/", HidriaWeb.Shopfloor, as: :shopfloor do
  end

  scope "/", HidriaWeb.Defsol, as: :defsol do
    pipe_through([:browser, :operator, :state])
    resources("/errors", ErrorController, except: [:show, :delete])
    resources("/causes", CauseController, only: [:index, :show, :update])
    resources("/solutions", SolutionController, only: [:index, :show])
    resources("/reports", ReportController, except: [:show])
    resources("/notes", NoteController, except: [:show])
  end

  scope "/admin", HidriaWeb.Admin, as: :admin do
    pipe_through([:browser, :lead])

    resources("/parts", PartController, except: [:show])
    resources("/errors", ErrorController, except: [:show])
    post("/errors/:id/clone", ErrorController, :clone)
    resources("/causes", CauseController, except: [:show])
    resources("/solutions", SolutionController, except: [:show])
    resources("/reports", ReportController)
    resources("/notes", NoteController)
  end

  scope "/admin", HidriaWeb.Admin, as: :admin do
    pipe_through([:browser, :admin])

    resources("/users", UserController)
    resources("/stations", StationController, except: [:show])
    get("/users/:id/newpassword", UserController, :new_password)
    put("/users/:id/updatepassword", UserController, :update_password)
  end

  # Other scopes may use custom stacks.
  scope "/api", HidriaWeb do
    pipe_through(:api)
    post("/attachments", Defsol.AttachmentController, :create)
  end
end
