defmodule HidriaWeb.Plugs.SelfControl do
  import Phoenix.Controller, only: [put_flash: 3, redirect: 2]
  import HidriaWeb.Router.Helpers

  def init(opts) do
    opts
  end

  def call(
        conn = %{assigns: %{current_user: user}, params: %{"id" => resource_id}},
        {module, function}
      ) do
    resource = apply(module, function, [resource_id])
    check(conn, user.id, resource.author.id)
  end

  def call(conn = %{assigns: %{current_user: user}, params: %{"id" => user_id}}, _) do
    check(conn, user.id, user_id)
  end

  def call(conn, _) do
    conn |> not_allowed()
  end

  def check(conn, user_id, author_id) do
    case user_id == author_id do
      true -> conn
      _ -> conn |> not_allowed()
    end
  end

  defp not_allowed(conn) do
    conn
    |> put_flash(:error, "You can't modify other people resources!")
    |> redirect(to: home_path(conn, :index))
    |> Plug.Conn.halt()
  end
end
