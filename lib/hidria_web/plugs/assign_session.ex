defmodule HidriaWeb.Plugs.AssignSession do
  import Plug.Conn

  alias Hidria.Shopfloor

  def init(opts) do
    opts
  end

  def call(conn, opts) do
    assign_session(conn, opts)
  end

  defp assign_session(conn, []), do: conn

  defp assign_session(conn, [key | tail]) do
    conn |> assign_session(key) |> assign_session(tail)
  end

  defp assign_session(conn, key) do
    resource = conn |> get_session(key) |> to_resource(key)
    conn |> assign(key, resource)
  end

  defp to_resource(resource = %{__struct__: _}, _), do: Map.from_struct(resource)
  defp to_resource(id, :current_station) when is_binary(id), do: Shopfloor.get_station!(id)
  defp to_resource(id, :current_part) when is_binary(id), do: Shopfloor.get_part!(id)
  defp to_resource(rest, _), do: rest
end
