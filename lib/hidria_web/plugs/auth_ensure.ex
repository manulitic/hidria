defmodule HidriaWeb.Plugs.AuthEnsure do
  alias Phoenix.Controller
  alias HidriaWeb.Router.Helpers

  def init(opts) do
    opts
  end

  def call(conn = %{assigns: %{current_user: _}}, _) do
    conn
  end

  def call(conn, _) do
    conn
    |> Controller.put_flash(:error, "You are not authenticated!")
    |> Controller.redirect(to: Helpers.login_path(conn, :new, redirect_to: conn.request_path))
    |> Plug.Conn.halt()
  end
end
