defmodule HidriaWeb.Plugs.AuthRole do
  alias Phoenix.Controller
  alias HidriaWeb.Router.Helpers

  alias Hidria.Accounts

  def init(opts) do
    opts
  end

  def call(conn = %{assigns: %{current_user: user}}, role) do
    case Accounts.check_role_access?(user, role) do
      true ->
        conn

      _ ->
        conn
        |> Controller.put_flash(:error, "You do not have the correct authorization!")
        |> Controller.redirect(to: Helpers.home_path(conn, :index))
    end
  end
end
