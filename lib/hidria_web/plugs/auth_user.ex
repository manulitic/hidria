defmodule HidriaWeb.Plugs.AuthUser do
  import Plug.Conn

  alias Hidria.Accounts

  def init(opts) do
    opts
  end

  def call(conn, _) do
    with {:ok, id} <- get_user_id(conn),
         {:ok, user} <- Accounts.get_user(id) do
      conn |> assign(:current_user, user)
    else
      {:error, _} ->
        conn

      _ ->
        conn
    end
  end

  @doc """
  First check the conn assigns before fetching the session.
  This allows for easier testing.
  """
  def get_user_id(conn) do
    case conn.assigns[:current_user_id] do
      nil -> get_from_session(conn, :current_user_id)
      id -> {:ok, id}
    end
  end

  def get_from_session(conn, key) do
    case get_session(conn, key) do
      nil -> {:error, "no user id in session"}
      id -> {:ok, id}
    end
  end
end
