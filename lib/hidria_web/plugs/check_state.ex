defmodule HidriaWeb.Plugs.CheckState do
  alias Phoenix.Controller
  alias HidriaWeb.Router.Helpers

  def init(opts) do
    opts
  end

  def call(conn = %{assigns: %{current_station: value}}, :station) do
    case value do
      nil ->
        redirect(conn, :station)
        |> Plug.Conn.halt()

      _ ->
        conn
    end
  end

  def call(conn = %{assigns: %{current_part: value}}, :part) do
    case value do
      nil ->
        redirect(conn, :part)
        |> Plug.Conn.halt()

      _ ->
        conn
    end
  end

  def redirect(conn, :station) do
    conn
    |> Controller.put_flash(:info, "Set your working machine!")
    |> Controller.redirect(
      to: Helpers.state_path(conn, :show, "station", redirect_to: conn.request_path)
    )
  end

  def redirect(conn, :part) do
    conn
    |> Controller.put_flash(:info, "Set the working part code!")
    |> Controller.redirect(
      to: Helpers.state_path(conn, :show, "part", redirect_to: conn.request_path)
    )
  end
end
