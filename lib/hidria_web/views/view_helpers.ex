defmodule HidriaWeb.ViewHelpers do
  import HidriaWeb.Router.Helpers
  alias Phoenix.HTML

  @doc """
  render shared templates
  """
  def render_shared(template, assigns \\ []) do
    Phoenix.View.render(HidriaWeb.SharedView, template, assigns)
  end

  @doc """
  Used to inject search parameters in scrivener html pagination
  """
  def pagination_opts(conn) do
    case search(conn) do
      nil -> []
      query -> [search: query]
    end
  end

  @doc """
  Get the search term from conn, used by `pagination_opts/1`
  """
  def search(conn) do
    Map.get(conn.assigns, :search)
  end

  @doc """
  Used by `LayoutView.state/1` to give links for reseting the state
  """
  def state_delete_path(_conn, nil, _), do: nil

  def state_delete_path(conn, _, resource) do
    state_path(conn, :delete, resource, redirect_to: conn.request_path)
  end

  @doc """
  Link to delete current station from state
  """
  def state_delete_station(conn) do
    state_delete_path(conn, Map.get(conn.assigns, :current_station), "station")
  end

  @doc """
  Link to delete current part from state
  """
  def state_delete_part(conn) do
    state_delete_path(conn, Map.get(conn.assigns, :current_part), "part")
  end

  @doc """
  Used by `LayoutView.state/1` to show resource code as label
  """
  def resource_code(conn, resource, key) do
    conn.assigns
    |> get_in([resource])
    |> get_key(key)
  end

  defp get_key(nil, _), do: "Not selected"
  defp get_key(resource, key), do: resource |> get_in([Access.key(key)])

  @doc """
  get an association field
  """
  def get(nil, _), do: ""
  def get(source, key), do: Map.get(source, key)

  @doc """
  select2 class
  """
  def select_class, do: [class: "select2-class form-control"]

  @doc """

  """
  def select_options(nil), do: select_class()
  def select_options(value) when is_binary(value), do: [selected: value] ++ select_class()
  def select_options(resource), do: [selected: resource.id] ++ select_class()

  @doc """
  Construct a list of params containing `:part_id` and `:station_id` to use
  as args for route helpers.
  """
  def station_part_params(conn) do
    station_id = get_in(conn.assigns, [:current_station]) |> get_struct_id()
    part_id = get_in(conn.assigns, [:current_part]) |> get_struct_id()

    keyid(station_id, :station_id) ++ keyid(part_id, :part_id)
  end

  def get_struct_id(%{id: id}), do: id
  def get_struct_id(id), do: id

  def keyid(nil, _), do: []
  def keyid(id, key), do: Keyword.new() |> Keyword.put(key, id)

  @doc """
  Open or close grid row
  """
  def row(action, condition)
  def row(:open, true), do: "<div class=\"row\">" |> HTML.raw()
  def row(:open, _), do: nil
  def row(:close, true), do: "</div>" |> HTML.raw()
  def row(:close, _), do: nil

  @doc """
  DateTime parsing for inserted_at
  """
  def inserted_at(%{inserted_at: d}), do: time_format(d)

  @doc """
  DateTime parsing for updated_at
  """
  def updated_at(%{updated_at: d}), do: time_format(d)

  defp time_format(d) do
    timezone = Application.get_env(:hidria, :timezone, "Europe/Rome")
    dt = Timex.to_datetime(d, timezone)
    Timex.format!(dt, "{D}.{0M}.{YYYY} {h24}:{m}")
  end
end
