defmodule HidriaWeb.Defsol.AttachmentView do
  use HidriaWeb, :view

  def render("index.json", %{attachments: attachments}) do
    %{data: render_many(attachments, __MODULE__, "attachment.json")}
  end

  def render("show.json", %{attachment: attachment}) do
    %{data: render_one(attachment, __MODULE__, "attachment.json")}
  end

  def render("attachment.json", %{attachment: attachment}) do
    %{
      id: attachment.id,
      error_id: attachment.error_id,
      fileinfo: attachment.fileinfo,
      inserted_at: attachment.inserted_at,
      updated_at: attachment.updated_at
    }
  end

  def render("errors.json", %{errors: e}) do
    errors = Enum.map(e, fn {error, points} -> %{type: error, datapoints: points} end)
    %{data: errors}
  end
end
