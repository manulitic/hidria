defmodule HidriaWeb.NoteView.Helpers do
  alias HidriaWeb.Router.Helpers, as: Routes

  import Phoenix.HTML.Link, only: [link: 2]

  @tabs ~w[errors causes solutions]

  def tab_navigation(conn, path_fn) do
    tabs = @tabs |> Enum.map(&one_tab(conn, path_fn, &1))
    all_path = apply(Routes, path_fn, [conn, :index])

    [
      %{
        active: tab_active?(conn, "all"),
        href: all_path,
        label: "All"
      }
    ] ++ tabs
  end

  def one_tab(conn, path_fn, value) do
    href = apply(Routes, path_fn, [conn, :index, [association: value]])

    %{
      active: tab_active?(conn, value),
      href: href,
      label: String.capitalize(value)
    }
  end

  def tab_active?(%{query_params: %{"association" => value}}, value), do: true

  def tab_active?(%{query_params: %{"association" => value}}, "all") do
    case value in @tabs do
      true -> false
      _ -> true
    end
  end

  def tab_active?(_conn, "all"), do: true
  def tab_active?(_conn, _value), do: false

  def association(nil, _), do: ""
  def association(%{title: title}, label), do: label <> title

  def association_link(nil, _, _), do: ""

  def association_link(item = %{title: title}, f, conn) do
    path = f.(conn, :edit, item)
    link(title, to: path, class: "btn btn-secondary btn-sm")
  end
end
