defmodule HidriaWeb.Admin.StationView do
  use HidriaWeb, :view

  def selected_parts(%{station: station}), do: Enum.map(station.parts, fn p -> p.id end)
  def selected_parts(_), do: []
end
