defmodule HidriaWeb.Admin.PartView do
  use HidriaWeb, :view

  def selected_stations(%{part: part}), do: Enum.map(part.stations, fn s -> s.id end)
  def selected_stations(_), do: []
end
