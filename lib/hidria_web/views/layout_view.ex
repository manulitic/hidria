defmodule HidriaWeb.LayoutView do
  use HidriaWeb, :view
  use Pumba

  def state(conn) do
    [
      %{
        label: "Machine: #{resource_code(conn, :current_station, :description)}",
        href: state_path(conn, :show, "station", redirect_to: conn.request_path),
        delete: state_delete_station(conn)
      },
      %{
        label: "Part Code: #{resource_code(conn, :current_part, :code)}",
        href: state_path(conn, :show, "part", redirect_to: conn.request_path),
        delete: state_delete_part(conn)
      }
    ]
  end

  def user(conn = %{assigns: %{current_user: user}}) do
    %Pumba.Components.User{
      fullname: user.fullname,
      profile_link: user_path(conn, :show, user),
      sign_out_link: login_path(conn, :delete)
    }
  end

  def user(conn) do
    %Pumba.Components.User{
      fullname: "Not signed in",
      profile_link: login_path(conn, :new)
    }
  end

  def menu(conn = %{assigns: %{current_user: user}}), do: get_menu(conn, user.role)
  def menu(conn), do: get_menu(conn, "operator")

  def get_menu(conn, "admin") do
    [
      problem_solving_menu(conn),
      administration: [
        {:child, :admin_user_path, [:index], "Users", "fa fa-users"},
        {:child, :admin_station_path, [:index], "Machines", "fa fa-desktop"},
        {:child, :admin_part_path, [:index], "Part Codes", "fa fa-hashtag"},
        {:child, :admin_error_path, [:index], "Errors", "fa fa-thumbs-down"},
        {:child, :admin_cause_path, [:index], "Causes", "fa fa-compass"},
        {:child, :admin_solution_path, [:index], "Solutions", "fa fa-crosshairs"},
        {:child, :admin_report_path, [:index], "Reports", "fa fa-send-o"},
        {:child, :admin_note_path, [:index], "Notes", "fa fa-sticky-note"}
      ]
    ]
  end

  def get_menu(conn, "lead") do
    [
      problem_solving_menu(conn),
      administration: [
        {:child, :admin_part_path, [:index], "Part Codes", "fa fa-hashtag"},
        {:child, :admin_error_path, [:index], "Errors", "fa fa-thumbs-down"},
        {:child, :admin_cause_path, [:index], "Causes", "fa fa-compass"},
        {:child, :admin_solution_path, [:index], "Solutions", "fa fa-crosshairs"},
        {:child, :admin_report_path, [:index], "Reports", "fa fa-send-o"},
        {:child, :admin_note_path, [:index], "Notes", "fa fa-sticky-note"}
      ]
    ]
  end

  def get_menu(conn, "operator") do
    [problem_solving_menu(conn)]
  end

  def problem_solving_menu(conn) do
    {:"Problem Solving",
     [
       {:child, :home_path, [:index], "Production", "fa fa-home"},
       {:child, :defsol_error_path, [:index, station_part_params(conn)], "Errors",
        "fa fa-thumbs-down"},
       {:child, :defsol_cause_path, [:index], "Causes", "fa fa-compass"},
       {:child, :defsol_report_path, [:index], "Reports", "fa fa-send-o"},
       {:child, :defsol_note_path, [:index], "Notes", "fa fa-sticky-note"}
     ]}
  end
end
