defmodule HidriaWeb.UserView do
  use HidriaWeb, :view

  def role(%{role: role}) do
    role = String.downcase(role)

    case role do
      "operator" -> "Operator"
      "lead" -> "Technical Leader"
      "admin" -> "Administrator"
    end
  end
end
