defmodule HidriaWeb.StateController do
  use HidriaWeb, :controller

  alias Hidria.Shopfloor

  def show(conn, params = %{"resource" => "station"}) do
    stations = Shopfloor.list_stations(params)
    search = get_in(params, ["search"])

    render(
      conn,
      "stations.html",
      [stations: stations, search: search] ++ redirect_to(params)
    )
  end

  def show(conn, params = %{"resource" => "part"}) do
    case get_session(conn, :current_station) do
      nil ->
        conn
        |> redirect(to: state_path(conn, :show, "station"))

      station ->
        params = params |> Enum.into(%{"station_id" => station_id(station)})
        parts = Shopfloor.list_parts(params)
        search = get_in(params, ["search"])

        render(
          conn,
          "parts.html",
          [parts: parts, search: search] ++ redirect_to(params)
        )
    end
  end

  defp station_id(%{id: id}), do: id
  defp station_id(id) when is_binary(id), do: id

  def create(conn, params = %{"station" => id}) do
    station = Shopfloor.get_station!(id).id

    conn
    |> put_session(:current_station, station)
    |> redirect(to: back_to(conn, params))
  end

  def create(conn, params = %{"part" => id}) do
    part = Shopfloor.get_part!(id).id

    conn
    |> put_session(:current_part, part)
    |> redirect(to: back_to(conn, params))
  end

  def create(conn, params) do
    conn
    |> put_flash(:error, "Unable to process request!")
    |> redirect(to: back_to(conn, params))
  end

  def delete(conn, params = %{"resource" => key}) do
    conn
    |> delete_session(key_to_current(key))
    |> redirect(to: back_to(conn, params))
  end

  def delete_all(conn) do
    conn
    |> delete_session(:current_part)
    |> delete_session(:current_station)
  end

  defp key_to_current(key, prefix \\ "current_") do
    prefix <> key
  end
end
