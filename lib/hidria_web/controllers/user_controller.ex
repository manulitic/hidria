defmodule HidriaWeb.UserController do
  use HidriaWeb, :controller

  alias Hidria.Accounts
  alias HidriaWeb.Plugs.SelfControl
  @roles Accounts.roles()

  plug(SelfControl)

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    render(conn, "edit.html", user: user, changeset: changeset, roles: @roles)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: user_path(conn, :show, user))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset, roles: @roles)
    end
  end

  def new_password(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    action = user_path(conn, :update_password, user)
    render(conn, "form_password.html", user: user, changeset: changeset, action: action)
  end

  def update_password(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_password(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User password changed successfully.")
        |> redirect(to: user_path(conn, :show, user))

      {:error, %Ecto.Changeset{} = changeset} ->
        action = user_path(conn, :update_password, user)
        render(conn, "form_password.html", user: user, changeset: changeset, action: action)
    end
  end
end
