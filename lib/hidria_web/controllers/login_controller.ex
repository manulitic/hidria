defmodule HidriaWeb.LoginController do
  use HidriaWeb, :controller

  alias Hidria.Accounts.User
  alias Hidria.Accounts.Auth
  alias HidriaWeb.StateController

  plug(:put_layout, "login.html")

  def new(conn, _) do
    render(conn, "new.html")
  end

  def create(conn, %{"username" => username, "password" => password}) do
    case Auth.by_username(username, password) do
      {:ok, user} ->
        to = role_redirect(conn, user)
        IO.puts("LoginController -> redirecting to: #{to}")

        conn
        |> put_flash(:info, "Succesfully logged in!")
        |> put_session(:current_user_id, user.id)
        |> redirect(to: to)

      {:error, _} ->
        conn
        |> put_flash(:error, "Wrong credentials, please try again")
        |> redirect(to: login_path(conn, :new))
    end
  end

  def delete(conn, _) do
    conn
    |> put_flash(:info, "Succesfully logged out!")
    |> delete_session(:current_user_id)
    |> StateController.delete_all()
    |> redirect(to: login_path(conn, :new))
  end

  def role_redirect(conn, %User{role: role}) do
    case role do
      "admin" -> admin_station_path(conn, :index)
      "lead" -> admin_error_path(conn, :index)
      _ -> home_path(conn, :index)
    end
  end
end
