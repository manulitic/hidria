defmodule HidriaWeb.Defsol.ErrorController do
  use HidriaWeb, :controller

  alias Hidria.Defsol
  alias Hidria.Defsol.Error

  alias Hidria.Shopfloor

  def index(conn, params) do
    errors = Defsol.list_errors(params)
    search = get_in(params, ["search"])

    render(
      conn,
      "index.html",
      page: errors,
      search: search,
      parts: Shopfloor.list_parts(),
      stations: Shopfloor.list_stations()
    )
  end

  def new(conn, _params) do
    changeset = Defsol.change_error(%Error{active: true})
    render_new(conn, changeset)
  end

  def create(conn, %{"error" => error_params}) do
    params = merge_inserted_by(conn, error_params)

    case Defsol.create_error(params) do
      {:ok, error} ->
        conn
        |> put_flash(:info, "Error created successfully.")
        |> redirect(
          to:
            attachment_path(
              conn,
              :index,
              error_id: error.id,
              finish_path: home_path(conn, :index)
            )
        )

      {:error, %Ecto.Changeset{} = changeset} ->
        render_new(conn, changeset)
    end
  end

  defp render_new(conn, changeset) do
    render(
      conn,
      "new.html",
      changeset: changeset,
      parts: Shopfloor.list_parts(),
      stations: Shopfloor.list_stations()
    )
  end

  def edit(conn, %{"id" => id}) do
    error = Defsol.get_error!(id)
    changeset = Defsol.change_error(error)

    render(
      conn,
      "edit.html",
      error: error,
      changeset: changeset,
      parts: Shopfloor.list_parts(),
      stations: Shopfloor.list_stations()
    )
  end

  def update(conn, %{"id" => id, "error" => error_params}) do
    error = Defsol.get_error!(id)
    params = merge_updated_by(conn, error_params)

    case Defsol.update_error(error, params) do
      {:ok, _error} ->
        conn
        |> put_flash(:info, "Error updated successfully.")
        |> redirect(to: back_to(conn, error_params))

      {:error, _} ->
        conn
        |> put_flash(:error, "Unable to update error")
        |> redirect(to: back_to(conn, error_params))
    end
  end
end
