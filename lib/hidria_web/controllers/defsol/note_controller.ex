defmodule HidriaWeb.Defsol.NoteController do
  use HidriaWeb, :controller

  alias Hidria.Defsol
  alias Hidria.Defsol.Note

  alias HidriaWeb.Plugs.SelfControl
  plug(SelfControl, {Defsol, :get_note!} when action not in [:index, :new, :create])

  def index(conn, params) do
    notes =
      conn
      |> merge_inserted_by(params)
      |> Defsol.list_notes()

    search = get_in(params, ["search"])
    render(conn, "index.html", page: notes, search: search)
  end

  def new(conn, params) do
    changeset = Defsol.change_note(%Note{}, params)
    render(conn, "new.html", redirect_to(params) ++ [changeset: changeset])
  end

  def create(conn, all_params = %{"note" => note_params}) do
    params = merge_inserted_by(conn, note_params)

    case Defsol.create_note(params) do
      {:ok, _note} ->
        conn
        |> put_flash(:info, "Note created successfully.")
        |> redirect(to: back_to(conn, all_params))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    note = Defsol.get_note!(id)
    changeset = Defsol.change_note(note)
    render(conn, "edit.html", note: note, changeset: changeset)
  end

  def update(conn, %{"id" => id, "note" => note_params}) do
    note = Defsol.get_note!(id)
    params = merge_updated_by(conn, note_params)

    case Defsol.update_note(note, params) do
      {:ok, _note} ->
        conn
        |> put_flash(:info, "Note updated successfully.")
        |> redirect(to: defsol_note_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", note: note, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    note = Defsol.get_note!(id)
    {:ok, _note} = Defsol.delete_note(note)

    conn
    |> put_flash(:info, "Note deleted successfully.")
    |> redirect(to: defsol_note_path(conn, :index))
  end
end
