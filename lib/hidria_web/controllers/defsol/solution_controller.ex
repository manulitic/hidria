defmodule HidriaWeb.Defsol.SolutionController do
  use HidriaWeb, :controller

  alias Hidria.Defsol

  def index(conn, params) do
    solutions = Defsol.list_solutions(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: solutions, search: search)
  end

  def show(conn, %{"id" => id}) do
    solution = Defsol.get_solution!(id)
    render(conn, "show.html", solution: solution)
  end
end
