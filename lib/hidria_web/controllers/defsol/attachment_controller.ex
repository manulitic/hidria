defmodule HidriaWeb.Defsol.AttachmentController do
  use HidriaWeb, :controller

  alias Hidria.Defsol

  def index(conn, params = %{"error_id" => assoc_id}) do
    attachments = Defsol.list_attachments(params)
    finish_path = Map.get(params, "finish_path", admin_error_path(conn, :index))
    render_index(conn, attachments, "error", assoc_id, finish_path)
  end

  def index(conn, params = %{"cause_id" => assoc_id}) do
    attachments = Defsol.list_attachments(params)
    finish_path = Map.get(params, "finish_path", admin_cause_path(conn, :index))
    render_index(conn, attachments, "cause", assoc_id, finish_path)
  end

  def index(conn, params = %{"solution_step_id" => assoc_id}) do
    attachments = Defsol.list_attachments(params)
    finish_path = Map.get(params, "finish_path", admin_solution_path(conn, :index))
    render_index(conn, attachments, "solution-step", assoc_id, finish_path)
  end

  def render_index(conn, attachments, assoc, assoc_id, finish_path) do
    render(
      conn,
      "index.html",
      attachments: attachments,
      assoc_id: assoc_id,
      assoc: assoc,
      delete: true,
      finish_path: finish_path
    )
  end

  def create(conn, params) do
    case Defsol.create_attachment(params) do
      {:ok, attachment} ->
        conn
        |> put_status(:ok)
        |> render("show.json", attachment: attachment)

      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(HidriaWeb.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, params = %{"id" => id}) do
    case Defsol.delete_attachment(id) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Attachment deleted succesfully!")
        |> redirect(to: back_to(conn, params))

      {:error, _} ->
        conn
        |> put_flash(:error, "Failed to delete the attachment!")
        |> redirect(to: back_to(conn, params))
    end
  end
end
