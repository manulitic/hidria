defmodule HidriaWeb.Defsol.CauseController do
  use HidriaWeb, :controller

  alias Hidria.Defsol

  def index(conn, params) do
    causes = Defsol.list_causes(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: causes, search: search)
  end

  def show(conn, %{"id" => id}) do
    cause = Defsol.get_cause!(id)
    render(conn, "show.html", cause: cause)
  end

  def update(conn, %{"id" => id, "cause" => cause_params}) do
    cause = Defsol.get_cause!(id)
    attrs = merge_updated_by(conn, cause_params)

    case Defsol.update_cause(cause, attrs) do
      {:ok, _cause} ->
        conn
        |> put_flash(:info, "Cause updated successfully.")
        |> redirect(to: back_to(conn, cause_params))

      {:error, _} ->
        conn
        |> put_flash(:error, "Unable to update Cause")
        |> redirect(to: back_to(conn, cause_params))
    end
  end
end
