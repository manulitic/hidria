defmodule HidriaWeb.HomeController do
  use HidriaWeb, :controller

  alias Hidria.Defsol

  def index(conn, params) do
    params =
      params
      |> station_params(conn)
      |> part_params(conn)

    active_errors = Defsol.list_active_errors(params)
    reports = Defsol.list_reports_by_shopfloor(params)
    render(conn, "index.html", active_errors: active_errors, reports: reports)
  end

  def station_params(params, %{assigns: %{current_station: station}}) do
    %{station_id: station.id} |> Enum.into(params)
  end

  def station_params(params, _conn), do: params

  def part_params(params, %{assigns: %{current_part: part}}) do
    %{part_id: part.id} |> Enum.into(params)
  end

  def part_params(params, _conn), do: params
end
