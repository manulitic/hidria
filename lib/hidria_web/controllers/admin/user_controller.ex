defmodule HidriaWeb.Admin.UserController do
  use HidriaWeb, :controller

  alias Hidria.Accounts
  alias Hidria.Accounts.User

  @roles Accounts.roles()

  def index(conn, params) do
    users = Accounts.list_users(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: users, search: search)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render(conn, "new.html", changeset: changeset, roles: @roles)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: admin_user_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, roles: @roles)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    render(conn, "edit.html", user: user, changeset: changeset, roles: @roles)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_user(user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: admin_user_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset, roles: @roles)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)

    case Accounts.delete_user(user) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User deleted successfully.")
        |> redirect(to: admin_user_path(conn, :index))

      {:error, changeset} ->
        conn
        |> put_flash(:error, get_errors(changeset))
        |> redirect(to: admin_user_path(conn, :index))
    end
  end

  defp get_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, _} -> msg end)
    |> Map.values()
    |> List.flatten()
    |> Enum.join()
  end

  def new_password(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    action = admin_user_path(conn, :update_password, user)
    render(conn, "form_password.html", user: user, changeset: changeset, action: action)
  end

  def update_password(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_password(user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User password changed successfully.")
        |> redirect(to: admin_user_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        action = admin_user_path(conn, :update_password, user)
        render(conn, "form_password.html", user: user, changeset: changeset, action: action)
    end
  end
end
