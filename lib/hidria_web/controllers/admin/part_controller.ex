defmodule HidriaWeb.Admin.PartController do
  use HidriaWeb, :controller

  alias Hidria.Shopfloor
  alias Hidria.Shopfloor.Part

  def index(conn, params) do
    parts = Shopfloor.list_parts(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: parts, search: search)
  end

  def new(conn, _params) do
    changeset = Shopfloor.change_part(%Part{})
    stations = Shopfloor.list_stations()
    render(conn, "new.html", changeset: changeset, stations: stations)
  end

  def create(conn, %{"part" => part_params}) do
    case Shopfloor.create_part(part_params) do
      {:ok, part} ->
        conn
        |> put_flash(:info, "Part #{part.code} created successfully.")
        |> redirect(to: admin_part_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        stations = Shopfloor.list_stations()
        render(conn, "new.html", changeset: changeset, stations: stations)
    end
  end

  def edit(conn, %{"id" => id}) do
    part = Shopfloor.get_part!(id)
    changeset = Shopfloor.change_part(part)
    stations = Shopfloor.list_stations()
    render(conn, "edit.html", part: part, changeset: changeset, stations: stations)
  end

  def update(conn, %{"id" => id, "part" => part_params}) do
    part = Shopfloor.get_part!(id)

    case Shopfloor.update_part(part, part_params) do
      {:ok, part} ->
        conn
        |> put_flash(:info, "Part #{part.code} updated successfully.")
        |> redirect(to: admin_part_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        stations = Shopfloor.list_stations()
        render(conn, "edit.html", part: part, changeset: changeset, stations: stations)
    end
  end

  def delete(conn, %{"id" => id}) do
    part = Shopfloor.get_part!(id)
    {:ok, part} = Shopfloor.delete_part(part)

    conn
    |> put_flash(:info, "Part #{part.code} deleted successfully.")
    |> redirect(to: admin_part_path(conn, :index))
  end
end
