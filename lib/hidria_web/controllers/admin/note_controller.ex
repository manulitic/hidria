defmodule HidriaWeb.Admin.NoteController do
  use HidriaWeb, :controller

  alias Hidria.Defsol
  alias Hidria.Defsol.Note

  def index(conn, params) do
    notes = Defsol.list_notes(params)
    search = get_in(params, ["search"])
    assigns = assigns() ++ [page: notes, search: search]
    render(conn, "index.html", assigns)
  end

  def new(conn, params) do
    changeset = Defsol.change_note(%Note{}, params)
    assigns = assigns() ++ [changeset: changeset]
    render(conn, "new.html", assigns)
  end

  def create(conn, %{"note" => note_params}) do
    params = merge_inserted_by(conn, note_params)

    case Defsol.create_note(params) do
      {:ok, _note} ->
        conn
        |> put_flash(:info, "Note created successfully.")
        |> redirect(to: admin_note_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        assigns = assigns() ++ [changeset: changeset]
        render(conn, "new.html", assigns)
    end
  end

  def show(conn, %{"id" => id}) do
    note = Defsol.get_note!(id)
    render(conn, "show.html", note: note)
  end

  def edit(conn, %{"id" => id}) do
    note = Defsol.get_note!(id)
    changeset = Defsol.change_note(note)
    assigns = assigns() ++ [note: note, changeset: changeset]
    render(conn, "edit.html", assigns)
  end

  def update(conn, %{"id" => id, "note" => note_params}) do
    note = Defsol.get_note!(id)
    params = merge_updated_by(conn, note_params)

    case Defsol.update_note(note, params) do
      {:ok, _note} ->
        conn
        |> put_flash(:info, "Note updated successfully.")
        |> redirect(to: admin_note_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        assigns = assigns() ++ [note: note, changeset: changeset]
        render(conn, "edit.html", assigns)
    end
  end

  def delete(conn, %{"id" => id}) do
    note = Defsol.get_note!(id)
    {:ok, _note} = Defsol.delete_note(note)

    conn
    |> put_flash(:info, "Note deleted successfully.")
    |> redirect(to: admin_note_path(conn, :index))
  end

  def assigns do
    [
      errors: Defsol.list_errors(),
      causes: Defsol.list_causes(),
      solutions: Defsol.list_solutions()
    ]
  end
end
