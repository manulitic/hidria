defmodule HidriaWeb.Admin.CauseController do
  use HidriaWeb, :controller

  alias Hidria.Defsol
  alias Hidria.Defsol.Cause

  def index(conn, params) do
    causes = Defsol.list_causes(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: causes, search: search)
  end

  def new(conn, params) do
    changeset = Defsol.change_cause(%Cause{}, params)
    errors = Defsol.list_errors()
    render(conn, "new.html", changeset: changeset, errors: errors)
  end

  def create(conn, params = %{"cause" => cause_params}) do
    attrs = merge_inserted_by(conn, cause_params)

    case Defsol.create_cause(attrs) do
      {:ok, cause} ->
        conn
        |> put_flash(:info, "Cause created successfully.")
        |> redirect(to: redirect_path(conn, cause, params))

      {:error, %Ecto.Changeset{} = changeset} ->
        errors = Defsol.list_errors()
        render(conn, "new.html", changeset: changeset, errors: errors)
    end
  end

  def redirect_path(conn, cause, %{"action" => "attachments"}) do
    admin_cause_path(conn, :edit, cause)
  end

  def redirect_path(conn, cause, %{"action" => "solution"}) do
    admin_solution_path(conn, :new, cause_id: cause.id)
  end

  def redirect_path(conn, _, _), do: admin_cause_path(conn, :index)

  def edit(conn, %{"id" => id}) do
    cause = Defsol.get_cause!(id)
    changeset = Defsol.change_cause(cause)
    errors = Defsol.list_errors()
    render(conn, "edit.html", cause: cause, changeset: changeset, errors: errors)
  end

  def update(conn, params = %{"id" => id, "cause" => cause_params}) do
    cause = Defsol.get_cause!(id)
    attrs = merge_updated_by(conn, cause_params)

    case Defsol.update_cause(cause, attrs) do
      {:ok, cause} ->
        conn
        |> put_flash(:info, "Cause updated successfully.")
        |> redirect(to: redirect_path(conn, cause, params))

      {:error, %Ecto.Changeset{} = changeset} ->
        errors = Defsol.list_errors()
        render(conn, "edit.html", cause: cause, changeset: changeset, errors: errors)
    end
  end

  def delete(conn, %{"id" => id}) do
    cause = Defsol.get_cause!(id)
    {:ok, _cause} = Defsol.delete_cause(cause)

    conn
    |> put_flash(:info, "Cause deleted successfully.")
    |> redirect(to: admin_cause_path(conn, :index))
  end
end
