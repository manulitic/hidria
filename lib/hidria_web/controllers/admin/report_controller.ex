defmodule HidriaWeb.Admin.ReportController do
  use HidriaWeb, :controller

  alias Hidria.Defsol
  alias Hidria.Defsol.Report

  def index(conn, params) do
    reports = Defsol.list_reports(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: reports, search: search)
  end

  def new(conn, params) do
    changeset = Defsol.change_report(%Report{}, params)
    solutions = Defsol.list_solutions()
    render(conn, "new.html", changeset: changeset, solutions: solutions)
  end

  def create(conn, %{"report" => report_params}) do
    params = merge_inserted_by(conn, report_params)

    case Defsol.create_report(params) do
      {:ok, _report} ->
        conn
        |> put_flash(:info, "Report created successfully.")
        |> redirect(to: admin_report_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        case get_in(params, ["solution_id"]) do
          nil ->
            conn
            |> put_flash(:error, "Missing solution identifier!")
            |> redirect(to: admin_report_path(conn, :index))

          solution_id ->
            solution = Defsol.get_solution!(solution_id)
            render(conn, "new.html", changeset: changeset, solution: solution)
        end
    end
  end

  def show(conn, %{"id" => id}) do
    report = Defsol.get_report!(id)
    render(conn, "show.html", report: report)
  end

  def edit(conn, %{"id" => id}) do
    report = Defsol.get_report!(id)
    changeset = Defsol.change_report(report)
    solutions = Defsol.list_solutions()
    render(conn, "edit.html", report: report, changeset: changeset, solutions: solutions)
  end

  def update(conn, %{"id" => id, "report" => report_params}) do
    report = Defsol.get_report!(id)
    params = merge_updated_by(conn, report_params)

    case Defsol.update_report(report, params) do
      {:ok, _report} ->
        conn
        |> put_flash(:info, "Report updated successfully.")
        |> redirect(to: admin_report_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        solutions = Defsol.list_solutions()
        render(conn, "edit.html", report: report, changeset: changeset, solutions: solutions)
    end
  end

  def delete(conn, %{"id" => id}) do
    report = Defsol.get_report!(id)
    {:ok, _report} = Defsol.delete_report(report)

    conn
    |> put_flash(:info, "Report deleted successfully.")
    |> redirect(to: admin_report_path(conn, :index))
  end
end
