defmodule HidriaWeb.Admin.SolutionController do
  use HidriaWeb, :controller

  alias Hidria.Defsol
  alias Hidria.Defsol.Solution

  def index(conn, params) do
    solutions = Defsol.list_solutions(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: solutions, search: search)
  end

  def new(conn, params) do
    changeset = Defsol.new_solution(params)
    causes = Defsol.list_causes()
    render(conn, "new.html", changeset: changeset, causes: causes)
  end

  def create(conn, %{"action" => "addstep", "solution" => solution_params}) do
    changeset = Defsol.change_solution(%Solution{}, add_step(solution_params))
    causes = Defsol.list_causes()
    render(conn, "new.html", changeset: changeset, causes: causes)
  end

  def create(conn, %{"action" => "remove_step_" <> i, "solution" => solution_params}) do
    changeset = Defsol.change_solution(%Solution{}, remove_step(solution_params, i))
    causes = Defsol.list_causes()
    render(conn, "new.html", changeset: changeset, causes: causes)
  end

  def create(conn, params = %{"solution" => solution_params}) do
    attrs = merge_inserted_by(conn, solution_params)

    case Defsol.create_solution(attrs) do
      {:ok, solution} ->
        conn
        |> put_flash(:info, "Solution created successfully.")
        |> redirect(to: redirect_path(conn, solution, params))

      {:error, %Ecto.Changeset{} = changeset} ->
        causes = Defsol.list_causes()
        render(conn, "new.html", changeset: changeset, causes: causes)
    end
  end

  def redirect_path(conn, cause, %{"action" => "attachments"}) do
    admin_solution_path(conn, :edit, cause)
  end

  def redirect_path(conn, _, _), do: admin_solution_path(conn, :index)

  def edit(conn, %{"id" => id}) do
    solution = Defsol.get_solution!(id)
    changeset = Defsol.change_solution(solution)
    causes = Defsol.list_causes()
    render(conn, "edit.html", solution: solution, changeset: changeset, causes: causes)
  end

  def update(conn, %{"id" => id, "action" => "addstep", "solution" => solution_params}) do
    solution = Defsol.get_solution!(id)

    changeset =
      solution
      |> Defsol.change_solution(add_step(solution_params))

    causes = Defsol.list_causes()
    render(conn, "edit.html", solution: solution, changeset: changeset, causes: causes)
  end

  def update(conn, %{"id" => id, "action" => "remove_step_" <> i, "solution" => solution_params}) do
    solution = Defsol.get_solution!(id)

    changeset =
      solution
      |> Defsol.change_solution(remove_step(solution_params, i))

    causes = Defsol.list_causes()
    render(conn, "edit.html", solution: solution, changeset: changeset, causes: causes)
  end

  def update(conn, params = %{"id" => id, "action" => "continue"}) do
    params =
      %{"redirect_to" => admin_solution_path(conn, :edit, id)}
      |> Map.merge(params)
      |> Map.drop(["action"])

    update(conn, params)
  end

  def update(conn, params = %{"id" => id, "solution" => solution_params}) do
    solution = Defsol.get_solution!(id)
    redirect_to = Map.get(params, "redirect_to", admin_solution_path(conn, :index))
    attrs = merge_updated_by(conn, solution_params)

    case Defsol.update_solution(solution, attrs) do
      {:ok, _solution} ->
        conn
        |> put_flash(:info, "Solution updated successfully.")
        |> redirect(to: redirect_to)

      {:error, %Ecto.Changeset{} = changeset} ->
        causes = Defsol.list_causes()
        render(conn, "edit.html", solution: solution, changeset: changeset, causes: causes)
    end
  end

  def delete(conn, %{"id" => id}) do
    solution = Defsol.get_solution!(id)
    {:ok, _solution} = Defsol.delete_solution(solution)

    conn
    |> put_flash(:info, "Solution deleted successfully.")
    |> redirect(to: admin_solution_path(conn, :index))
  end

  def add_step(solution_params) do
    steps =
      solution_params["steps"]
      |> Map.values()
      |> Kernel.++([%{"title" => "", "attachments" => []}])

    %{"steps" => steps} |> Enum.into(solution_params)
  end

  def remove_step(solution_params, i) do
    steps =
      solution_params["steps"]
      |> Enum.reject(&delete_step(&1, i))
      |> Enum.map(fn {_, v} -> v end)

    %{"steps" => steps} |> Enum.into(solution_params)
  end

  def delete_step({k, step}, k) do
    Defsol.delete_step_attachments(step["id"])
    true
  end

  def delete_step(_, _), do: false
end
