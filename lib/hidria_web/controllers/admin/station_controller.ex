defmodule HidriaWeb.Admin.StationController do
  use HidriaWeb, :controller

  alias Hidria.Shopfloor
  alias Hidria.Shopfloor.Station

  def index(conn, params) do
    stations = Shopfloor.list_stations(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: stations, search: search)
  end

  def new(conn, _params) do
    changeset = Shopfloor.change_station(%Station{})
    parts = Shopfloor.list_parts()
    render(conn, "new.html", changeset: changeset, parts: parts)
  end

  def create(conn, %{"station" => station_params}) do
    case Shopfloor.create_station(station_params) do
      {:ok, _station} ->
        conn
        |> put_flash(:info, "Station created successfully.")
        |> redirect(to: admin_station_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        parts = Shopfloor.list_parts()
        render(conn, "new.html", changeset: changeset, parts: parts)
    end
  end

  def edit(conn, %{"id" => id}) do
    station = Shopfloor.get_station!(id)
    parts = Shopfloor.list_parts()
    changeset = Shopfloor.change_station(station)
    render(conn, "edit.html", station: station, parts: parts, changeset: changeset)
  end

  def update(conn, %{"id" => id, "station" => station_params}) do
    station = Shopfloor.get_station!(id)

    case Shopfloor.update_station(station, station_params) do
      {:ok, _station} ->
        conn
        |> put_flash(:info, "Station updated successfully.")
        |> redirect(to: admin_station_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        parts = Shopfloor.list_parts()
        render(conn, "edit.html", station: station, parts: parts, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    station = Shopfloor.get_station!(id)
    {:ok, _station} = Shopfloor.delete_station(station)

    conn
    |> put_flash(:info, "Station deleted successfully.")
    |> redirect(to: admin_station_path(conn, :index))
  end
end
