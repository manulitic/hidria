defmodule HidriaWeb.Admin.ErrorController do
  use HidriaWeb, :controller

  alias Hidria.Defsol
  alias Hidria.Defsol.Error

  alias Hidria.Shopfloor

  def index(conn, params) do
    errors = Defsol.list_errors(params)
    search = get_in(params, ["search"])
    render(conn, "index.html", page: errors, search: search)
  end

  def new(conn, _params) do
    changeset = Defsol.change_error(%Error{})
    render_new(conn, changeset)
  end

  def create(conn, all_params = %{"error" => error_params}) do
    params = merge_inserted_by(conn, error_params)

    case Defsol.create_error(params) do
      {:ok, error} ->
        conn
        |> put_flash(:info, "Error created successfully.")
        |> redirect(to: redirect_path(conn, error, all_params))

      {:error, %Ecto.Changeset{} = changeset} ->
        render_new(conn, changeset)
    end
  end

  def redirect_path(conn, error, %{"action" => "attachments"}) do
    admin_error_path(conn, :edit, error)
  end

  def redirect_path(conn, error, %{"action" => "cause"}) do
    admin_cause_path(conn, :new, error_id: error.id)
  end

  def redirect_path(conn, _, _), do: admin_error_path(conn, :index)

  defp render_new(conn, changeset) do
    render(
      conn,
      "new.html",
      changeset: changeset,
      parts: Shopfloor.list_parts(),
      stations: Shopfloor.list_stations()
    )
  end

  def edit(conn, %{"id" => id}) do
    error = Defsol.get_error!(id)
    changeset = Defsol.change_error(error)

    render_edit(conn, error, changeset)
  end

  def clone(conn, %{"id" => id}) do
    error = Defsol.clone_error(id) |> Defsol.preload_error()
    changeset = Defsol.change_error(error)

    conn
    |> put_flash(:info, "Successfully cloned error!")
    |> render_edit(error, changeset)
  end

  def update(conn, all_params = %{"id" => id, "error" => error_params}) do
    error = Defsol.get_error!(id)
    params = merge_updated_by(conn, error_params)

    case Defsol.update_error(error, params) do
      {:ok, error} ->
        conn
        |> put_flash(:info, "Error updated successfully.")
        |> redirect(to: redirect_path(conn, error, all_params))

      {:error, %Ecto.Changeset{} = changeset} ->
        render_edit(conn, error, changeset)
    end
  end

  defp render_edit(conn, error, changeset) do
    render(
      conn,
      "edit.html",
      error: error,
      changeset: changeset,
      parts: Shopfloor.list_parts(),
      stations: Shopfloor.list_stations()
    )
  end

  def delete(conn, %{"id" => id}) do
    error = Defsol.get_error!(id)
    {:ok, _error} = Defsol.delete_error(error)

    conn
    |> put_flash(:info, "Error deleted successfully.")
    |> redirect(to: admin_error_path(conn, :index))
  end
end
