defmodule HidriaWeb.ControllerHelpers do
  import HidriaWeb.Router.Helpers

  def redirect_to(%{"redirect_to" => href}), do: [redirect_to: href]
  def redirect_to(_), do: []

  def back_to(_conn, %{"redirect_to" => href}), do: href
  def back_to(conn, _), do: home_path(conn, :index)

  def merge_inserted_by(conn, params) do
    Map.merge(params, %{"inserted_by" => conn.assigns[:current_user].id})
  end

  def merge_updated_by(conn, params) do
    Map.merge(params, %{"updated_by" => conn.assigns[:current_user].id})
  end
end
