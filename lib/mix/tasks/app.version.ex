defmodule Mix.Tasks.App.Version do
  @moduledoc """
  Get the current value for the project version.
  ```
  Usage: mix app.version
  ```
  """
  use Mix.Task

  # task properties
  @shortdoc "print the project version"

  def run(_args) do
    version =
      Mix.Project.config()
      |> Keyword.get(:version)

    # respond in the shell
    Mix.shell().info("#{version}")
  end
end
