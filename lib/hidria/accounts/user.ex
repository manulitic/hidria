defmodule Hidria.Accounts.User do
  use Ecto.Schema
  use Arc.Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  alias Hidria.Accounts
  alias Hidria.Avatar

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field(:fullname, :string)
    field(:password_hash, :string)
    field(:role, :string)
    field(:username, :string)
    field(:avatar, Avatar.Type)

    field(:password, :string, virtual: true)
    field(:password_confirmation, :string, virtual: true)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :fullname, :role])
    |> cast_attachments(attrs, [:avatar])
    |> validate_required([:username, :fullname, :role])
    |> unique_constraint(:username)
    |> validate_inclusion(:role, Accounts.roles())
  end

  def create_changeset(user, attrs) do
    user
    |> changeset(attrs)
    |> cast(attrs, [:password, :password_confirmation])
    |> validate_password_confirmation(:password, :password_confirmation)
    |> put_pass_hash()
  end

  def validate_password_confirmation(changeset, field, confirmation, options \\ []) do
    validate_change(changeset, field, fn _, password ->
      case password == get_field(changeset, confirmation) do
        true -> []
        _ -> [{field, options[:message] || "Passwords do not match"}]
      end
    end)
  end

  defp put_pass_hash(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    changeset
    |> change(Comeonin.Bcrypt.add_hash(password))
    |> change(%{password_confirmation: nil})
  end

  defp put_pass_hash(changeset), do: changeset

  def search(query, nil), do: query

  def search(query, term) do
    wildcard = "%#{term}%"

    from(
      item in query,
      where: ilike(item.username, ^wildcard),
      or_where: ilike(item.fullname, ^wildcard)
    )
  end
end
