defmodule Hidria.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  import Ecto.Changeset, only: [add_error: 3]
  alias Hidria.Repo

  alias Hidria.Accounts.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  def list_users(params) do
    search_term = get_in(params, ["search"])

    User
    |> User.search(search_term)
    |> order_by(asc: :username)
    |> Repo.paginate(params)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  def get_user(id) do
    case Repo.get(User, id) do
      nil -> {:error, "no user"}
      user -> {:ok, user}
    end
  end

  def get_by_username(username) do
    Repo.get_by(User, username: username)
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    id = Ecto.UUID.generate()

    %User{id: id}
    |> User.create_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Change user password
  """
  def update_password(%User{} = user, attrs) do
    user
    |> User.create_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{role: "admin"} = user) do
    admins =
      from(u in User, where: u.role == "admin", select: count(u.id))
      |> Repo.one()

    delete_admin(user, admins > 1)
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def delete_admin(user, true), do: Repo.delete(user)

  def delete_admin(user, _) do
    changeset =
      user
      |> change_user()
      |> add_error(:role, "Can't delete the last admin!")

    {:error, changeset}
  end

  def delete_all_users do
    Repo.delete_all(User)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  @roles [operator: 0, lead: 1, admin: 2]
  def roles, do: @roles |> Keyword.keys() |> Enum.map(&Atom.to_string/1)

  def role_level(key) when is_binary(key) do
    key = String.to_atom(key)
    @roles |> Keyword.fetch(key)
  end

  def role_level(key), do: @roles |> Keyword.fetch(key)

  def check_role_access?(user, role) do
    role_level(user.role) >= role_level(role)
  end
end
