defmodule Hidria.Accounts.Auth do
  alias Hidria.Accounts
  alias Comeonin.Bcrypt

  def by_username(username, password) do
    case Accounts.get_by_username(username) do
      nil ->
        Bcrypt.dummy_checkpw()
        {:error, "wrong credentials"}

      user ->
        Bcrypt.check_pass(user, password)
    end
  end
end
