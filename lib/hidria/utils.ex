defmodule Hidria.Utils do
  import Ecto.Changeset, only: [cast: 3]

  def get_params(params, key) when is_atom(key) do
    with nil <- get_in(params, [key]),
         binary_key <- Atom.to_string(key) do
      get_in(params, [binary_key])
    else
      value -> value
    end
  end

  @doc """
  Update `updated_by` only if changeset is valid and changes have occured
  """
  def cast_updated_by(changeset = %{changes: changes, valid?: true}, attrs) do
    case Enum.empty?(changes) do
      true -> changeset
      false -> changeset |> cast(attrs, [:updated_by])
    end
  end

  def cast_updated_by(changeset, _), do: changeset
end
