defprotocol Clone do
  @doc "Clones the structure with all its associations"
  def clone(data, params \\ %{})

  @doc "Checks if two structures are clones"
  def is_clone?(data, clone)
end
