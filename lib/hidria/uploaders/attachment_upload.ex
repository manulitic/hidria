defmodule Hidria.AttachmentUpload do
  use Arc.Definition
  use Arc.Ecto.Definition

  @versions [:original, :thumb]

  def __storage, do: Arc.Storage.Local

  @extension_whitelist ~w(.jpg .jpeg .gif .png)

  def validate({file, _}) do
    file_extension = file.file_name |> Path.extname() |> String.downcase()
    Enum.member?(@extension_whitelist, file_extension)
  end

  def transform(:thumb, _) do
    {:convert, "-strip -thumbnail 250x250^ -gravity center -extent 250x250 -format png", :png}
  end

  def transform(:original, _) do
    {:convert, "-resize 1200x1200\> -format png", :png}
  end

  def filename(version, _), do: version

  def storage_dir(_version, {_file, scope}) do
    "uploads/attachments/#{scope.filehash}"
  end
end
