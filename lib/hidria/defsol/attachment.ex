defmodule Hidria.Defsol.Attachment do
  use Ecto.Schema
  use Arc.Ecto.Schema

  import Ecto.Changeset

  alias Hidria.Defsol

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "attachments" do
    field(:fileinfo, Hidria.AttachmentUpload.Type)
    field(:filehash, :string)
    belongs_to(:error, Defsol.Error)
    belongs_to(:cause, Defsol.Cause)
    belongs_to(:solution_step, Defsol.SolutionStep)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(attachment, params \\ %{}) do
    attachment
    |> cast(params, [:error_id, :cause_id, :solution_step_id])
    |> cast_attachments(params, [:fileinfo], allow_paths: true)
  end

  @doc false
  def clone_changeset(attachment, params \\ %{}) do
    attachment
    |> cast(params, [:error_id, :cause_id, :solution_step_id, :fileinfo, :filehash])
  end

  defimpl Clone do
    alias Hidria.Defsol.Attachment
    alias Hidria.Repo

    def clone(data, params) do
      data = data |> Map.from_struct() |> Map.merge(params)

      %Attachment{}
      |> Attachment.clone_changeset(data)
      |> Repo.insert!()
    end

    def is_clone?(data = %{fileinfo: fi, filehash: fh}, clone = %{fileinfo: fi, filehash: fh}) do
      with true <- data.id != clone.id,
           true <- data.inserted_at != clone.inserted_at,
           true <- data.updated_at != clone.updated_at do
        true
      else
        _ -> false
      end
    end

    def is_clone?(_, _), do: false
  end
end
