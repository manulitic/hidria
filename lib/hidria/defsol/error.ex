defmodule Hidria.Defsol.Error do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  import Hidria.Utils, only: [cast_updated_by: 2]

  alias Hidria.Shopfloor
  alias Hidria.Defsol
  alias Hidria.Accounts.User

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "errors" do
    field(:title, :string)
    field(:description, :string)
    field(:active, :boolean)
    belongs_to(:part, Shopfloor.Part)
    belongs_to(:station, Shopfloor.Station)
    has_many(:causes, Defsol.Cause)
    has_many(:attachments, Defsol.Attachment, on_replace: :delete)

    belongs_to(:author, User, foreign_key: :inserted_by)
    belongs_to(:editor, User, foreign_key: :updated_by)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(error, attrs) do
    error
    |> cast(attrs, [:title, :description, :active, :station_id, :part_id, :inserted_by])
    |> cast_updated_by(attrs)
    |> validate_required([:title])
  end

  def search(query, nil), do: query

  def search(query, term) do
    wildcard = "%#{term}%"

    from(item in query, where: ilike(item.title, ^wildcard))
  end

  defimpl Clone do
    alias Hidria.Defsol
    alias Hidria.Repo

    def clone(data, params) do
      {:ok, clone} =
        data
        |> Map.from_struct()
        |> Map.merge(params)
        |> Defsol.create_error()

      data = data |> Repo.preload([:causes, :attachments])

      Enum.each(data.causes, fn a ->
        Clone.clone(a, %{error_id: clone.id})
      end)

      Enum.each(data.attachments, fn a ->
        Clone.clone(a, %{error_id: clone.id})
      end)

      clone
    end

    def is_clone?(data, clone) do
      with true <- data.id != clone.id,
           true <- data.inserted_at != clone.inserted_at,
           true <- data.updated_at != clone.updated_at,
           true <- data.active == clone.active,
           true <- data.description == clone.description,
           true <- data.title == clone.title do
        true
      else
        _ -> false
      end
    end
  end
end
