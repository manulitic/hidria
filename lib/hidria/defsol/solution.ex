defmodule Hidria.Defsol.Solution do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  import Hidria.Utils, only: [cast_updated_by: 2]

  alias Hidria.Defsol.Cause
  alias Hidria.Defsol.SolutionStep
  alias Hidria.Accounts.User

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "solutions" do
    field(:description, :string)
    field(:title, :string)
    belongs_to(:cause, Cause)
    has_many(:steps, SolutionStep, on_replace: :delete)

    belongs_to(:author, User, foreign_key: :inserted_by)
    belongs_to(:editor, User, foreign_key: :updated_by)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(solution, attrs) do
    solution
    |> cast(attrs, [:title, :description, :cause_id, :inserted_by])
    |> cast_assoc(:steps, required: true)
    |> cast_updated_by(attrs)
    |> validate_required([:title, :cause_id])
  end

  @doc false
  def clone_changeset(solution, attrs) do
    solution
    |> cast(attrs, [:title, :description, :cause_id, :inserted_by, :updated_by])
    |> validate_required([:title, :cause_id])
  end

  def search(query, nil), do: query

  def search(query, term) do
    wildcard = "%#{term}%"

    from(
      item in query,
      where: ilike(item.title, ^wildcard),
      or_where: ilike(item.description, ^wildcard)
    )
  end

  defimpl Clone do
    alias Hidria.Defsol.Solution
    alias Hidria.Defsol
    alias Hidria.Repo

    def clone(data, params) do
      params = data |> Map.from_struct() |> Map.merge(params)

      clone =
        %Solution{}
        |> Solution.clone_changeset(params)
        |> Repo.insert!()

      data = data |> Defsol.preload_steps()

      Enum.each(data.steps, fn x ->
        Clone.clone(x, %{solution_id: clone.id})
      end)

      clone
    end

    def is_clone?(data = %{title: t, description: d}, clone = %{title: t, description: d}) do
      with true <- data.id != clone.id,
           true <- data.inserted_at != clone.inserted_at,
           true <- data.updated_at != clone.updated_at do
        true
      else
        _ -> false
      end
    end

    def is_clone?(_, _), do: false
  end
end
