defmodule Hidria.Defsol.Report do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  import Hidria.Utils, only: [cast_updated_by: 2]

  alias Hidria.Defsol.Solution
  alias Hidria.Accounts.User

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "reports" do
    field(:description, :string)
    field(:success, :boolean, default: true)
    belongs_to(:solution, Solution)
    has_one(:cause, through: [:solution, :cause])
    has_one(:error, through: [:solution, :cause, :error])

    belongs_to(:author, User, foreign_key: :inserted_by)
    belongs_to(:editor, User, foreign_key: :updated_by)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(report, attrs) do
    report
    |> cast(attrs, [:description, :success, :solution_id, :inserted_by])
    |> cast_updated_by(attrs)
    |> validate_required([:success, :solution_id, :inserted_by])
  end

  def search(query, nil), do: query

  def search(query, term) do
    wildcard = "%#{term}%"

    from(
      item in query,
      where: ilike(item.description, ^wildcard)
    )
  end
end
