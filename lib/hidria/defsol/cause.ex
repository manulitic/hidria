defmodule Hidria.Defsol.Cause do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  import Hidria.Utils, only: [cast_updated_by: 2]

  alias Hidria.Defsol
  alias Hidria.Accounts.User

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "causes" do
    field(:active, :boolean, default: false)
    field(:description, :string)
    field(:detection, :string)
    field(:title, :string)
    belongs_to(:error, Defsol.Error)
    has_many(:attachments, Defsol.Attachment, on_replace: :delete)
    has_many(:solutions, Defsol.Solution, on_delete: :delete_all)

    belongs_to(:author, User, foreign_key: :inserted_by)
    belongs_to(:editor, User, foreign_key: :updated_by)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(cause, attrs) do
    cause
    |> cast(attrs, [:title, :description, :detection, :active, :error_id, :inserted_by])
    |> cast_updated_by(attrs)
    |> validate_required([:title, :detection, :error_id])
  end

  def search(query, nil), do: query

  def search(query, term) do
    wildcard = "%#{term}%"

    from(
      item in query,
      where: ilike(item.title, ^wildcard),
      or_where: ilike(item.description, ^wildcard),
      or_where: ilike(item.detection, ^wildcard)
    )
  end

  defimpl Clone do
    alias Hidria.Defsol
    alias Hidria.Repo

    def clone(data, params) do
      {:ok, clone} =
        data
        |> Map.from_struct()
        |> Map.merge(params)
        |> Defsol.create_cause()

      data = data |> Repo.preload([:solutions, :attachments])

      Enum.each(data.solutions, fn a ->
        Clone.clone(a, %{cause_id: clone.id})
      end)

      Enum.each(data.attachments, fn a ->
        Clone.clone(a, %{cause_id: clone.id})
      end)

      clone
    end

    def is_clone?(data, clone) do
      with true <- data.id != clone.id,
           true <- data.inserted_at != clone.inserted_at,
           true <- data.updated_at != clone.updated_at,
           true <- data.active == clone.active,
           true <- data.description == clone.description,
           true <- data.detection == clone.detection do
        true
      else
        _ -> false
      end
    end
  end
end
