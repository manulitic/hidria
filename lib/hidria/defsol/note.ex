defmodule Hidria.Defsol.Note do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  import Hidria.Utils, only: [cast_updated_by: 2]

  alias Hidria.Defsol
  alias Hidria.Accounts.User

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "notes" do
    field(:description, :string)

    belongs_to(:error, Defsol.Error)
    belongs_to(:cause, Defsol.Cause)
    belongs_to(:solution, Defsol.Solution)

    belongs_to(:author, User, foreign_key: :inserted_by)
    belongs_to(:editor, User, foreign_key: :updated_by)

    timestamps(type: :utc_datetime)
  end

  @allowed_attrs [:description, :error_id, :cause_id, :solution_id, :inserted_by]
  @doc false
  def changeset(note, attrs) do
    note
    |> cast(attrs, @allowed_attrs)
    |> cast_updated_by(attrs)
    |> validate_required([:description, :inserted_by])
  end

  @refers [:error, :cause, :solution]
  def on(note) do
    note
    |> Map.take(@refers)
    |> Enum.reject(fn {_, v} -> v == nil end)
    |> Enum.map(fn {k, _} -> Atom.to_string(k) end)
    |> refers()
  end

  def refers([term]) when is_binary(term), do: term

  def refers(term) when is_list(term) do
    term |> Enum.join(" & ")
  end

  def by(note) do
    note.user.fullname
  end

  def search(query, nil), do: query

  def search(query, term) do
    wildcard = "%#{term}%"

    from(
      item in query,
      where: ilike(item.description, ^wildcard)
    )
  end
end
