defmodule Hidria.Defsol do
  @moduledoc """
  The Defsol context.
  """

  import Ecto.Query, warn: false
  import Hidria.Utils
  alias Hidria.Repo

  alias Hidria.Defsol.Error

  @doc """
  Returns the list of errors.
  """
  def list_errors do
    Repo.all(Error)
  end

  def list_errors(params) do
    search_term = get_in(params, ["search"])

    Error
    |> errors_by_params(params)
    |> Error.search(search_term)
    |> order_by(asc: :title)
    |> preload([:part, :station, :causes, :attachments, :author, :editor])
    |> Repo.paginate(params)
  end

  @doc """
  List active errors
  """
  def list_active_errors(params \\ %{}) do
    Error
    |> errors_by_params(params)
    |> where(active: true)
    |> order_by(asc: :title)
    |> preload([:part, :station, :causes, :attachments, :author, :editor])
    |> Repo.paginate(params)
  end

  def errors_by_params(query, params \\ %{}) do
    part_id = get_params(params, :part_id)
    station_id = get_params(params, :station_id)

    query
    |> errors_by_station_id(station_id)
    |> errors_by_part_id(part_id)
  end

  def errors_by_part_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query
  def errors_by_part_id(query, id), do: from(e in query, where: e.part_id == ^id)

  def errors_by_station_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query
  def errors_by_station_id(query, id), do: from(e in query, where: e.station_id == ^id)

  @doc """
  Gets a single error.

  Raises `Ecto.NoResultsError` if the Error does not exist.
  """
  def get_error!(id) do
    Error
    |> Repo.get!(id)
    |> preload_error()
  end

  @doc """
  Creates a error.
  """
  def create_error(attrs \\ %{}) do
    %Error{}
    |> Error.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Clone error. Wrapper around `Clone.clone/2` to run in a transaction
  """
  def clone_error(%Error{} = error) do
    {:ok, clone} = Repo.transaction(fn -> Clone.clone(error) end)
    clone
  end

  def clone_error(id) when is_binary(id) do
    id |> get_error!() |> clone_error()
  end

  @doc """
  Preload error association for editing
  """
  def preload_error(error) do
    error |> Repo.preload([:part, :station, :causes, :attachments, :author, :editor])
  end

  @doc """
  Updates a error.
  """
  def update_error(%Error{} = error, attrs) do
    error
    |> Error.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Error.
  """
  def delete_error(%Error{} = error) do
    error = Repo.preload(error, :attachments)

    Repo.transaction(fn ->
      error.attachments |> Enum.each(&delete_attachment(&1))
      Repo.delete!(error)
    end)
  end

  def delete_all_errors do
    Repo.delete_all(Error)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking error changes.
  """
  def change_error(%Error{} = error, attrs \\ %{}) do
    Error.changeset(error, attrs)
  end

  alias Hidria.Defsol.Cause

  @doc """
  Returns the list of causes.
  """
  def list_causes do
    Repo.all(Cause)
  end

  def list_causes(params) do
    search_term = get_in(params, ["search"])

    Cause
    |> Cause.search(search_term)
    |> cause_by_error_query(params)
    |> order_by(asc: :title)
    |> preload([:error, :attachments, :author, :editor])
    |> Repo.paginate(params)
  end

  def cause_by_error_query(query, %{"error_id" => error_id}) do
    query |> where(error_id: ^error_id)
  end

  def cause_by_error_query(query, _), do: query

  @doc """
  Gets a single cause.
  """
  def get_cause!(id) do
    Cause
    |> Repo.get!(id)
    |> Repo.preload([:error, :attachments, :author, :editor])
  end

  @doc """
  Creates a cause.
  """
  def create_cause(attrs \\ %{}) do
    %Cause{}
    |> Cause.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a cause.
  """
  def update_cause(cause, attrs = %{active: true}), do: update_cause_and_error(cause, attrs)
  def update_cause(cause, attrs = %{"active" => true}), do: update_cause_and_error(cause, attrs)
  def update_cause(cause, attrs = %{"active" => "true"}), do: update_cause_and_error(cause, attrs)

  def update_cause(%Cause{} = cause, attrs) do
    cause
    |> Cause.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Updates the cause and sets the same active state for error
  """
  def update_cause_and_error(%Cause{} = cause, attrs) do
    cause_changeset = cause |> Cause.changeset(attrs)
    error_attrs = attrs |> Map.take([:active, "active"])

    error_changeset =
      cause |> Repo.preload(:error) |> Map.get(:error) |> Error.changeset(error_attrs)

    Repo.transaction(fn ->
      cause = Repo.update!(cause_changeset)
      Repo.update!(error_changeset)
      Repo.preload(cause, :error, force: true)
    end)
  end

  @doc """
  Deletes a Cause.
  """
  def delete_cause(%Cause{} = cause) do
    cause = Repo.preload(cause, :attachments)

    Repo.transaction(fn ->
      cause.attachments |> Enum.each(&delete_attachment(&1))
      Repo.delete!(cause)
    end)
  end

  def delete_all_causes do
    Repo.delete_all(Cause)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking cause changes.
  """
  def change_cause(%Cause{} = cause, attrs \\ %{}) do
    Cause.changeset(cause, attrs)
  end

  alias Hidria.Defsol.Solution
  alias Hidria.Defsol.SolutionStep

  @doc """
  Returns the list of solutions.
  """
  def list_solutions do
    Repo.all(Solution)
    |> preload_steps()
  end

  def list_solutions(params) do
    search_term = get_in(params, ["search"])

    Solution
    |> list_by_cause_query(params)
    |> Solution.search(search_term)
    |> order_by(asc: :title)
    |> preload([:cause, :author, :editor])
    |> preload(steps: ^steps_query())
    |> Repo.paginate(params)
  end

  def list_by_cause_query(query, %{"cause_id" => cause_id}) do
    query |> where(cause_id: ^cause_id)
  end

  def list_by_cause_query(query, _), do: query

  @doc """
  Gets a single solution.

  Raises `Ecto.NoResultsError` if the Solution does not exist.
  """
  def get_solution!(id) do
    Solution
    |> Repo.get!(id)
    |> preload_steps()
  end

  @doc """
  preload steps in an orderly manner
  """
  def steps_query do
    from(s in SolutionStep, order_by: :inserted_at, preload: [:attachments])
  end

  def preload_steps(result) do
    result
    |> Repo.preload(
      steps: from(s in SolutionStep, order_by: :inserted_at, preload: [:attachments])
    )
  end

  @doc """
  Creates a solution.
  """
  def create_solution(attrs \\ %{}) do
    %Solution{}
    |> Solution.changeset(attrs)
    |> Repo.insert()
  end

  def create_solution_step(attrs \\ %{}) do
    %SolutionStep{}
    |> SolutionStep.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Clone the solution with all its steps

  opts can have `:cause_id` to use as new parent
  """
  def clone_solution(solution, attrs \\ %{}) do
    solution
    |> Repo.preload(:steps)
    |> Map.drop([:id, :cause, :__meta__, :__struct__])
    |> clone_solution_steps()
    |> Map.merge(attrs)
    |> create_solution()
  end

  def clone_solution_steps(attrs = %{steps: steps}) do
    steps = steps |> Enum.map(&clone_solution_step(&1))
    Map.merge(attrs, %{steps: steps})
  end

  def clone_solution_step(step) do
    step
    |> Repo.preload(:attachments)
    |> Map.drop([:id, :cause, :__meta__, :__struct__])
  end

  @doc """
  Updates a solution.
  """
  def update_solution(%Solution{} = solution, attrs) do
    solution
    |> Solution.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Solution.
  """
  def delete_solution(%Solution{} = solution) do
    solution = solution |> preload_steps()

    Repo.transaction(fn ->
      solution.steps
      |> Enum.each(fn step ->
        step.attachments
        |> Enum.each(&delete_attachment(&1))
      end)

      Repo.delete!(solution)
    end)
  end

  @doc """
  Used by solution controller to remove dynamically steps
  """
  def delete_step_attachments(id) when is_binary(id) do
    SolutionStep
    |> Repo.get!(id)
    |> delete_step_attachments()
  end

  def delete_step_attachments(%SolutionStep{} = step) do
    step = Repo.preload(step, :attachments)

    step.attachments
    |> Enum.each(&delete_attachment(&1))
  end

  def delete_step_attachments(_), do: :ok

  def delete_all_solutions do
    Repo.delete_all(Solution)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking solution changes.
  """
  def change_solution(%Solution{} = solution, attrs \\ %{}) do
    Solution.changeset(solution, attrs)
  end

  @doc """
  Create a new solution with one empty step
  """
  def new_solution(attrs \\ %{}) do
    one_step = %{"steps" => [%{}]}
    solution = %Solution{} |> preload_steps()
    change_solution(solution, Map.merge(attrs, one_step))
  end

  @doc """
  Add a new step to existing solution
  """
  def add_step_to_solution(solution, step \\ %{}) do
    steps =
      solution.steps
      |> Enum.map(&Map.from_struct(&1))
      |> Kernel.++([step])

    change_solution(solution, %{"steps" => steps})
  end

  alias Hidria.Defsol.Report

  @doc """
  Returns the list of reports.
  """
  def list_reports do
    Report
    |> order_by(desc: :inserted_at)
    |> preload([:author, :editor, :solution])
    |> Repo.all()
  end

  def list_reports(params) do
    search_term = get_in(params, ["search"])

    Report
    |> resource_by_inserted_by(params)
    |> report_by_solution_query(params)
    |> order_by(desc: :inserted_at)
    |> Report.search(search_term)
    |> preload([:author, :editor, :solution])
    |> Repo.paginate(params)
  end

  def report_by_solution_query(query, %{"solution_id" => solution_id}) do
    query |> where(solution_id: ^solution_id)
  end

  def report_by_solution_query(query, _), do: query

  @doc """
  List reports based on part_id and station_id
  """
  def list_reports_by_shopfloor(params) do
    Report
    |> report_by_shopfloor(params)
    |> order_by(desc: :inserted_at)
    |> preload([:author, :editor, :solution])
    |> Repo.paginate(params)
  end

  def report_by_shopfloor(query, params) do
    station_id = get_params(params, :station_id)
    part_id = get_params(params, :part_id)

    query
    |> report_by_station_id(station_id)
    |> report_by_part_id(part_id)
  end

  def report_by_station_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query

  def report_by_station_id(query, id) do
    from(
      r in query,
      join: s in assoc(r, :solution),
      join: c in assoc(s, :cause),
      join: e in assoc(c, :error),
      where: e.station_id == ^id
    )
  end

  def report_by_part_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query

  def report_by_part_id(query, id) do
    from(
      r in query,
      join: s in assoc(r, :solution),
      join: c in assoc(s, :cause),
      join: e in assoc(c, :error),
      where: e.part_id == ^id
    )
  end

  @doc """
  Gets a single report.
  """
  def get_report!(id) do
    Report
    |> Repo.get!(id)
    |> Repo.preload([:solution, :author, :editor])
  end

  @doc """
  Creates a report.
  """
  def create_report(attrs \\ %{}) do
    %Report{}
    |> Report.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a report.
  """
  def update_report(%Report{} = report, attrs) do
    report
    |> Report.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Report.
  """
  def delete_report(%Report{} = report) do
    Repo.delete(report)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking report changes.
  """
  def change_report(%Report{} = report, attrs \\ %{}) do
    Report.changeset(report, attrs)
  end

  @doc """
  Update active field on error and cause after report is created
  """
  def update_active(%Report{success: true} = report) do
    report =
      report
      |> Repo.preload([:cause, :error])

    update_cause(report.cause, %{active: false})
    update_error(report.error, %{active: false})
  end

  def update_active(%Report{} = report), do: report

  alias Hidria.Defsol.Note

  @doc """
  Returns the list of notes.
  """
  def list_notes do
    Note
    |> order_by(desc: :inserted_at)
    |> preload([:error, :cause, :solution, :author, :editor])
    |> Repo.all()
  end

  def list_notes(params) do
    search_term = get_in(params, ["search"])

    Note
    |> resource_by_inserted_by(params)
    |> notes_association_query(params)
    |> order_by(desc: :inserted_at)
    |> Note.search(search_term)
    |> preload([:error, :cause, :solution, :author, :editor])
    |> Repo.paginate(params)
  end

  def notes_association_query(query, params) do
    association = get_params(params, :association)

    case association do
      nil ->
        notes_association_id_query(query, params)

      value ->
        notes_association_group_query(query, value)
    end
  end

  def notes_association_group_query(query, "errors") do
    from(e in query, where: not is_nil(e.error_id))
  end

  def notes_association_group_query(query, "causes") do
    from(e in query, where: not is_nil(e.cause_id))
  end

  def notes_association_group_query(query, "solutions") do
    from(e in query, where: not is_nil(e.solution_id))
  end

  def notes_association_id_query(query, params) do
    error_id = get_params(params, :error_id)
    cause_id = get_params(params, :cause_id)
    solution_id = get_params(params, :solution_id)

    query
    |> notes_by_error_id(error_id)
    |> notes_by_cause_id(cause_id)
    |> notes_by_solution_id(solution_id)
  end

  def notes_by_error_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query
  def notes_by_error_id(query, id), do: from(e in query, where: e.error_id == ^id)

  def notes_by_cause_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query
  def notes_by_cause_id(query, id), do: from(e in query, where: e.cause_id == ^id)

  def notes_by_solution_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query
  def notes_by_solution_id(query, id), do: from(e in query, where: e.solution_id == ^id)

  @doc """
  Gets a single note.
  """
  def get_note!(id) do
    Note
    |> Repo.get!(id)
    |> Repo.preload([:error, :cause, :solution, :author, :editor])
  end

  @doc """
  Creates a note.
  """
  def create_note(attrs \\ %{}) do
    %Note{}
    |> Note.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a note.
  """
  def update_note(%Note{} = note, attrs) do
    note
    |> Note.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Note.
  """
  def delete_note(%Note{} = note) do
    Repo.delete(note)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking note changes.
  """
  def change_note(%Note{} = note, attrs \\ %{}) do
    Note.changeset(note, attrs)
  end

  @doc """
  Query to filter resource by inserted_by
  """
  def resource_by_inserted_by(query, %{"inserted_by" => author_id}) do
    query |> where(inserted_by: ^author_id)
  end

  def resource_by_inserted_by(query, _), do: query

  alias Hidria.Defsol.Attachment
  alias Hidria.AttachmentUpload

  @doc """
  List attachments by association
  """
  def list_attachments(params \\ %{}) do
    Attachment
    |> attachments_association_query(params)
    |> order_by(asc: :inserted_at)
    |> Repo.all()
  end

  def attachments_association_query(query, params) do
    error_id = get_params(params, :error_id)
    cause_id = get_params(params, :cause_id)
    solution_step_id = get_params(params, :solution_step_id)

    query
    |> attachment_by_error_id(error_id)
    |> attachment_by_cause_id(cause_id)
    |> attachment_by_solution_step_id(solution_step_id)
  end

  def attachment_by_error_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query
  def attachment_by_error_id(query, id), do: from(e in query, where: e.error_id == ^id)

  def attachment_by_cause_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query
  def attachment_by_cause_id(query, id), do: from(e in query, where: e.cause_id == ^id)

  def attachment_by_solution_step_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query

  def attachment_by_solution_step_id(query, id),
    do: from(e in query, where: e.solution_step_id == ^id)

  @doc """
  Get the attachment
  """
  def get_attachment!(id) do
    Repo.get!(Attachment, id)
  end

  @doc """
  Create attachment
  """
  def create_attachment(attrs \\ %{}) do
    hash = attachment_hash(attrs)

    %Attachment{filehash: hash}
    |> Attachment.changeset(attrs)
    |> Repo.insert()
  end

  def attachment_hash(%{"fileinfo" => fileinfo}) do
    :crypto.hash(:md5, File.read!(fileinfo.path)) |> Base.encode16(case: :lower)
  end

  def attachment_hash(%{fileinfo: fileinfo}) do
    :crypto.hash(:md5, File.read!(fileinfo.path)) |> Base.encode16(case: :lower)
  end

  def attachment_hash(_), do: nil

  @doc """
  Delete attachment
  """
  def delete_attachment(%Attachment{} = attachment) do
    query = from(a in Attachment, where: a.filehash == ^attachment.filehash, select: count(a.id))

    if Repo.one(query) <= 1 do
      :ok = AttachmentUpload.delete({attachment.fileinfo, attachment})
      :ok = AttachmentUpload.storage_dir(nil, {nil, attachment}) |> File.rmdir()
    end

    Repo.delete(attachment)
  end

  def delete_attachment(id) when is_binary(id) do
    item = Repo.get!(Attachment, id)
    delete_attachment(item)
  end
end
