defmodule Hidria.Defsol.SolutionStep do
  use Ecto.Schema
  import Ecto.Changeset

  alias Hidria.Defsol

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "solutions_steps" do
    field(:description, :string)
    field(:title, :string)
    belongs_to(:solution, Defsol.Solution)
    has_many(:attachments, Defsol.Attachment, on_replace: :delete)

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(solution_step, attrs) do
    solution_step
    |> cast(attrs, [:title, :description, :solution_id])
    |> validate_required([:title])
  end

  defimpl Clone do
    alias Hidria.Defsol
    alias Hidria.Repo

    def clone(data, params) do
      {:ok, clone} =
        data
        |> Map.from_struct()
        |> Map.merge(params)
        |> Defsol.create_solution_step()

      data = data |> Repo.preload(:attachments)

      Enum.each(data.attachments, fn a ->
        Clone.clone(a, %{solution_step_id: clone.id})
      end)

      clone
    end

    def is_clone?(data = %{title: t, description: d}, clone = %{title: t, description: d}) do
      with true <- data.id != clone.id,
           true <- data.inserted_at != clone.inserted_at,
           true <- data.updated_at != clone.updated_at do
        true
      else
        _ -> false
      end
    end

    def is_clone?(_, _), do: false
  end
end
