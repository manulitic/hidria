defmodule Hidria.Shopfloor.Station do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  alias Hidria.Shopfloor.Part
  alias Hidria.Shopfloor
  alias Hidria.Utils

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "stations" do
    field(:code, :string)
    field(:description, :string)

    many_to_many(
      :parts,
      Part,
      join_through: "stations_parts",
      on_delete: :delete_all,
      on_replace: :delete
    )

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(station, attrs) do
    station
    |> cast(attrs, [:code, :description])
    |> validate_required([:code, :description])
    |> put_assoc(:parts, parse_parts(attrs))
  end

  def search(query, nil), do: query

  def search(query, term) do
    wildcard = "%#{term}%"

    from(
      item in query,
      where: ilike(item.code, ^wildcard),
      or_where: ilike(item.description, ^wildcard)
    )
  end

  def parse_parts(attrs) do
    (Utils.get_params(attrs, :parts) || [])
    |> Enum.map(&Shopfloor.get_part!(&1))
  end
end
