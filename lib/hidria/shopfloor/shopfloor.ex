defmodule Hidria.Shopfloor do
  @moduledoc """
  The shopfloor context.
  """

  import Ecto.Query, warn: false
  import Hidria.Utils
  alias Hidria.Repo

  alias Hidria.Shopfloor.Part
  alias Hidria.Shopfloor.Station

  @doc """
  Returns the list of parts.

  ## Examples

      iex> list_parts()
      [%Part{}, ...]

  """
  def list_parts do
    Repo.all(Part)
  end

  def list_parts(params) do
    search_term = get_in(params, ["search"])

    Part
    |> parts_by_params(params)
    |> Part.search(search_term)
    |> order_by(asc: :code)
    |> Repo.paginate(params)
  end

  def parts_by_params(query, params) do
    station_id = get_params(params, :station_id)

    query
    |> part_by_station_id(station_id)
  end

  def part_by_station_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query

  def part_by_station_id(query, id) do
    from(e in query, join: station in assoc(e, :stations), where: station.id == ^id)
  end

  @doc """
  Gets a single part.

  Raises `Ecto.NoResultsError` if the Part does not exist.

  ## Examples

      iex> get_part!(123)
      %Part{}

      iex> get_part!(456)
      ** (Ecto.NoResultsError)

  """
  def get_part!(id), do: Repo.get!(Part, id) |> Repo.preload(:stations)

  @doc """
  Creates a part.

  ## Examples

      iex> create_part(%{field: value})
      {:ok, %Part{}}

      iex> create_part(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_part(attrs \\ %{}) do
    %Part{}
    |> Part.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a part.

  ## Examples

      iex> update_part(part, %{field: new_value})
      {:ok, %Part{}}

      iex> update_part(part, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_part(%Part{} = part, attrs) do
    part
    |> Repo.preload(:stations)
    |> Part.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Part.

  ## Examples

      iex> delete_part(part)
      {:ok, %Part{}}

      iex> delete_part(part)
      {:error, %Ecto.Changeset{}}

  """
  def delete_part(%Part{} = part) do
    Repo.delete(part)
  end

  @doc """
  Delete all the parts
  """
  def delete_all_parts do
    Repo.delete_all(Part)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking part changes.

  ## Examples

      iex> change_part(part)
      %Ecto.Changeset{source: %Part{}}

  """
  def change_part(%Part{} = part) do
    Part.changeset(part, %{})
  end

  @doc """
  Returns the list of stations.

  ## Examples

      iex> list_stations()
      [%Station{}, ...]

  """
  def list_stations do
    Station
    |> order_by(asc: :description)
    |> Repo.all()
  end

  def list_stations(params) do
    search_term = get_in(params, ["search"])

    Station
    |> stations_by_params(params)
    |> Station.search(search_term)
    |> order_by(asc: :description)
    |> Repo.paginate(params)
  end

  def stations_by_params(query, params) do
    part_id = get_params(params, :part_id)

    query
    |> station_by_part_id(part_id)
  end

  def station_by_part_id(query, id) when is_nil(id) or byte_size(id) == 0, do: query

  def station_by_part_id(query, id) do
    from(e in query, join: part in assoc(e, :parts), where: part.id == ^id)
  end

  @doc """
  Gets a single station.

  Raises `Ecto.NoResultsError` if the Station does not exist.

  ## Examples

      iex> get_station!(123)
      %Station{}

      iex> get_station!(456)
      ** (Ecto.NoResultsError)

  """
  def get_station!(id), do: Repo.get!(Station, id) |> Repo.preload(:parts)

  @doc """
  Creates a station.

  ## Examples

      iex> create_station(%{field: value})
      {:ok, %Station{}}

      iex> create_station(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_station(attrs \\ %{}) do
    %Station{}
    |> Station.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a station.

  ## Examples

      iex> update_station(station, %{field: new_value})
      {:ok, %Station{}}

      iex> update_station(station, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_station(%Station{} = station, attrs) do
    station
    |> Repo.preload(:parts)
    |> Station.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Station.

  ## Examples

      iex> delete_station(station)
      {:ok, %Station{}}

      iex> delete_station(station)
      {:error, %Ecto.Changeset{}}

  """
  def delete_station(%Station{} = station) do
    Repo.delete(station)
  end

  def delete_all_stations do
    Repo.delete_all(Station)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking station changes.

  ## Examples

      iex> change_station(station)
      %Ecto.Changeset{source: %Station{}}

  """
  def change_station(%Station{} = station) do
    Station.changeset(station, %{})
  end
end
