defmodule Hidria.Shopfloor.Part do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  alias Hidria.Shopfloor.Station
  alias Hidria.Shopfloor
  alias Hidria.Utils

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "parts" do
    field(:code, :string)
    field(:description, :string)

    many_to_many(
      :stations,
      Station,
      join_through: "stations_parts",
      on_delete: :delete_all,
      on_replace: :delete
    )

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(part, attrs) do
    part
    |> cast(attrs, [:code, :description])
    |> validate_required([:code])
    |> unique_constraint(:code)
    |> put_assoc(:stations, parse_stations(attrs))
  end

  def search(query, nil), do: query

  def search(query, term) do
    wildcard = "%#{term}%"

    from(
      item in query,
      where: ilike(item.code, ^wildcard),
      or_where: ilike(item.description, ^wildcard)
    )
  end

  def parse_stations(attrs) do
    (Utils.get_params(attrs, :stations) || [])
    |> Enum.map(&Shopfloor.get_station!(&1))
  end
end
