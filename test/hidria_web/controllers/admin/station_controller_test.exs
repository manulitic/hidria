defmodule HidriaWeb.Admin.StationControllerTest do
  use HidriaWeb.AdminConnCase

  alias Hidria.Shopfloor

  @create_attrs %{code: "some code", description: "some description"}
  @update_attrs %{code: "some updated code", description: "some updated description"}
  @invalid_attrs %{code: nil, description: nil}

  def fixture(:station) do
    {:ok, station} = Shopfloor.create_station(@create_attrs)
    station
  end

  describe "index" do
    test "lists all stations", %{conn: conn} do
      conn = get(conn, admin_station_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Machines"
    end
  end

  describe "new station" do
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_station_path(conn, :new))
      assert html_response(conn, 200) =~ "New Machine"
    end
  end

  describe "create station" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, admin_station_path(conn, :create), station: @create_attrs)

      assert redirected_to(conn) == admin_station_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_station_path(conn, :create), station: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Machine"
    end
  end

  describe "edit station" do
    setup [:create_station]

    test "renders form for editing chosen station", %{conn: conn, station: station} do
      conn = get(conn, admin_station_path(conn, :edit, station))
      assert html_response(conn, 200) =~ "Edit Machine"
    end
  end

  describe "update station" do
    setup [:create_station]

    test "redirects when data is valid", %{conn: conn, station: station} do
      conn = put(conn, admin_station_path(conn, :update, station), station: @update_attrs)
      assert redirected_to(conn) == admin_station_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, station: station} do
      conn = put(conn, admin_station_path(conn, :update, station), station: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Machine"
    end
  end

  describe "delete station" do
    setup [:create_station]

    test "deletes chosen station", %{conn: conn, station: station} do
      conn = delete(conn, admin_station_path(conn, :delete, station))
      assert redirected_to(conn) == admin_station_path(conn, :index)
    end
  end

  defp create_station(_) do
    station = fixture(:station)
    {:ok, station: station}
  end
end
