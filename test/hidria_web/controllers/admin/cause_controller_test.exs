defmodule HidriaWeb.Admin.CauseControllerTest do
  use HidriaWeb.AdminConnCase
  import Hidria.Factory

  alias Hidria.Defsol

  @create_attrs %{
    active: true,
    description: "some description",
    detection: "some detection",
    title: "some title"
  }
  @update_attrs %{
    active: false,
    description: "some updated description",
    detection: "some updated detection",
    title: "some updated title"
  }
  @invalid_attrs %{active: nil, description: nil, detection: nil, title: nil}

  def fixture(:cause, attrs \\ %{}) do
    error = insert(:error)

    {:ok, cause} =
      attrs
      |> Enum.into(%{error_id: error.id})
      |> Enum.into(@create_attrs)
      |> Defsol.create_cause()

    cause
  end

  describe "index" do
    test "lists all causes", %{conn: conn} do
      conn = get(conn, admin_cause_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Causes"
    end
  end

  describe "new cause" do
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_cause_path(conn, :new))
      assert html_response(conn, 200) =~ "New Cause"
    end
  end

  describe "create cause" do
    test "redirects to index when no action is set", %{conn: conn} do
      params = %{error_id: insert(:error).id} |> Enum.into(@create_attrs)
      conn = post(conn, admin_cause_path(conn, :create), cause: params)
      assert redirected_to(conn) == admin_cause_path(conn, :index)
    end

    test "redirects to edit when action is attachments", %{conn: conn} do
      params = %{error_id: insert(:error).id} |> Enum.into(@create_attrs)
      conn = post(conn, admin_cause_path(conn, :create), cause: params, action: "attachments")

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == admin_cause_path(conn, :edit, id)
    end

    test "redirects to new solution when action is solution", %{conn: conn} do
      params = %{error_id: insert(:error).id} |> Enum.into(@create_attrs)
      conn = post(conn, admin_cause_path(conn, :create), cause: params, action: "solution")

      redirected_to = conn |> redirected_to() |> String.split("?") |> hd()
      assert redirected_to == admin_solution_path(conn, :new)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_cause_path(conn, :create), cause: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Cause"
    end
  end

  describe "edit cause" do
    setup [:create_cause]

    test "renders form for editing chosen cause", %{conn: conn, cause: cause} do
      conn = get(conn, admin_cause_path(conn, :edit, cause))
      assert html_response(conn, 200) =~ "Edit Cause"
    end
  end

  describe "update cause" do
    setup [:create_cause]

    test "redirects when data is valid", %{conn: conn, cause: cause} do
      conn = put(conn, admin_cause_path(conn, :update, cause), cause: @update_attrs)
      assert redirected_to(conn) == admin_cause_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, cause: cause} do
      conn = put(conn, admin_cause_path(conn, :update, cause), cause: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Cause"
    end
  end

  describe "delete cause" do
    setup [:create_cause]

    test "deletes chosen cause", %{conn: conn, cause: cause} do
      conn = delete(conn, admin_cause_path(conn, :delete, cause))
      assert redirected_to(conn) == admin_cause_path(conn, :index)
    end
  end

  defp create_cause(_) do
    cause = fixture(:cause)
    {:ok, cause: cause}
  end
end
