defmodule HidriaWeb.Admin.PartControllerTest do
  use HidriaWeb.AdminConnCase

  alias Hidria.Shopfloor

  @create_attrs %{code: "some code", description: "some description"}
  @update_attrs %{code: "some updated code", description: "some updated description"}
  @invalid_attrs %{code: nil, description: nil}

  def fixture(:part) do
    {:ok, part} = Shopfloor.create_part(@create_attrs)
    part
  end

  describe "index" do
    test "lists all parts", %{conn: conn} do
      conn = get(conn, admin_part_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Part Codes"
    end
  end

  describe "new part" do
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_part_path(conn, :new))
      assert html_response(conn, 200) =~ "New Part"
    end
  end

  describe "create part" do
    test "redirects to index when data is valid", %{conn: conn} do
      conn = post(conn, admin_part_path(conn, :create), part: @create_attrs)

      assert redirected_to(conn) == admin_part_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_part_path(conn, :create), part: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Part"
    end
  end

  describe "edit part" do
    setup [:create_part]

    test "renders form for editing chosen part", %{conn: conn, part: part} do
      conn = get(conn, admin_part_path(conn, :edit, part))
      assert html_response(conn, 200) =~ "Edit Part"
    end
  end

  describe "update part" do
    setup [:create_part]

    test "redirects to index when data is valid", %{conn: conn, part: part} do
      conn = put(conn, admin_part_path(conn, :update, part), part: @update_attrs)
      assert redirected_to(conn) == admin_part_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, part: part} do
      conn = put(conn, admin_part_path(conn, :update, part), part: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Part"
    end
  end

  describe "delete part" do
    setup [:create_part]

    test "deletes chosen part", %{conn: conn, part: part} do
      conn = delete(conn, admin_part_path(conn, :delete, part))
      assert redirected_to(conn) == admin_part_path(conn, :index)
    end
  end

  defp create_part(_) do
    part = fixture(:part)
    {:ok, part: part}
  end
end
