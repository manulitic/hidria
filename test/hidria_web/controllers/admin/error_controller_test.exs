defmodule HidriaWeb.Admin.ErrorControllerTest do
  use HidriaWeb.AdminConnCase

  alias Hidria.Defsol

  @create_attrs %{title: "some title"}
  @update_attrs %{title: "some updated title", active: true}
  @invalid_attrs %{title: nil}

  def fixture(:error) do
    {:ok, error} = Defsol.create_error(@create_attrs)
    error
  end

  describe "index" do
    test "lists all errors", %{conn: conn} do
      conn = get(conn, admin_error_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Errors"
    end
  end

  describe "new error" do
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_error_path(conn, :new))
      assert html_response(conn, 200) =~ "New Error"
    end
  end

  describe "create error" do
    test "redirects to edit with action is attachments", %{conn: conn} do
      conn =
        post(conn, admin_error_path(conn, :create), error: @create_attrs, action: "attachments")

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == admin_error_path(conn, :edit, id)
    end

    test "redirects to new cause with action: cause", %{conn: conn} do
      conn = post(conn, admin_error_path(conn, :create), error: @create_attrs, action: "cause")

      redirected_to = conn |> redirected_to() |> String.split("?") |> hd()
      assert redirected_to == admin_cause_path(conn, :new)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_error_path(conn, :create), error: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Error"
    end
  end

  describe "edit error" do
    setup [:create_error]

    test "renders form for editing chosen error", %{conn: conn, error: error} do
      conn = get(conn, admin_error_path(conn, :edit, error))
      assert html_response(conn, 200) =~ "Edit Error"
    end
  end

  describe "update error" do
    setup [:create_error]

    test "redirects when data is valid", %{conn: conn, error: error} do
      conn = put(conn, admin_error_path(conn, :update, error), error: @update_attrs)

      assert redirected_to(conn) == admin_error_path(conn, :index)
      error = Defsol.get_error!(error.id)
      assert error.active == true
    end

    test "renders errors when data is invalid", %{conn: conn, error: error} do
      conn = put(conn, admin_error_path(conn, :update, error), error: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Error"
    end
  end

  describe "delete error" do
    setup [:create_error]

    test "deletes chosen error", %{conn: conn, error: error} do
      conn = delete(conn, admin_error_path(conn, :delete, error))
      assert redirected_to(conn) == admin_error_path(conn, :index)
    end
  end

  defp create_error(_) do
    error = fixture(:error)
    {:ok, error: error}
  end
end
