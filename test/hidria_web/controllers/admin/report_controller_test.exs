defmodule HidriaWeb.Admin.ReportControllerTest do
  use HidriaWeb.AdminConnCase
  import Hidria.Factory

  alias Hidria.Defsol
  alias Hidria.Fixtures

  @create_attrs %{description: "some description", success: true}
  @update_attrs %{description: "some updated description", success: false}
  @invalid_attrs %{description: nil, success: nil}

  def user_fixture do
    Fixtures.user()
  end

  def fixture(:report) do
    {:ok, report} =
      %{}
      |> Enum.into(%{solution_id: insert(:solution).id})
      |> Enum.into(%{inserted_by: user_fixture().id})
      |> Enum.into(@create_attrs)
      |> Defsol.create_report()

    report
  end

  describe "index" do
    test "lists all reports", %{conn: conn} do
      conn = get(conn, admin_report_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Reports"
    end
  end

  describe "new report" do
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_report_path(conn, :new))
      assert html_response(conn, 200) =~ "New Report"
    end
  end

  describe "create report" do
    test "redirects to show when data is valid", %{conn: conn} do
      params =
        %{solution_id: insert(:solution).id}
        |> Enum.into(%{user_id: user_fixture().id})
        |> Enum.into(@create_attrs)

      post_conn = post(conn, admin_report_path(conn, :create), report: params)
      assert redirected_to(post_conn) == admin_report_path(post_conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_report_path(conn, :create), report: @invalid_attrs)
      assert html_response(conn, 302)
    end
  end

  describe "edit report" do
    setup [:create_report]

    test "renders form for editing chosen report", %{conn: conn, report: report} do
      conn = get(conn, admin_report_path(conn, :edit, report))
      assert html_response(conn, 200) =~ "Edit Report"
    end
  end

  describe "update report" do
    setup [:create_report]

    test "redirects when data is valid", %{conn: conn, report: report} do
      post_conn = put(conn, admin_report_path(conn, :update, report), report: @update_attrs)
      assert redirected_to(post_conn) == admin_report_path(post_conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, report: report} do
      conn = put(conn, admin_report_path(conn, :update, report), report: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Report"
    end
  end

  describe "delete report" do
    setup [:create_report]

    test "deletes chosen report", %{conn: conn, report: report} do
      post_conn = delete(conn, admin_report_path(conn, :delete, report))
      assert redirected_to(post_conn) == admin_report_path(post_conn, :index)

      assert_error_sent(404, fn ->
        get(conn, admin_report_path(conn, :show, report))
      end)
    end
  end

  defp create_report(_) do
    report = fixture(:report)
    {:ok, report: report}
  end
end
