defmodule HidriaWeb.Admin.UserControllerTest do
  use HidriaWeb.AdminConnCase

  alias Hidria.Accounts

  @create_attrs %{
    fullname: "some fullname",
    password_confirmation: "some password_hash",
    password: "some password_hash",
    role: "admin",
    username: "some username"
  }
  @update_attrs %{
    fullname: "some updated fullname",
    role: "admin",
    username: "some updated username"
  }
  @invalid_attrs %{fullname: nil, password_hash: nil, role: nil, username: nil}

  def fixture(:user, attrs \\ @create_attrs) do
    {:ok, user} = Accounts.create_user(attrs)
    user
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get(conn, admin_user_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Users"
    end
  end

  describe "new user" do
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_user_path(conn, :new))
      assert html_response(conn, 200) =~ "New User"
    end
  end

  describe "create user" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, admin_user_path(conn, :create), user: @create_attrs)

      assert redirected_to(conn) == admin_user_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_user_path(conn, :create), user: @invalid_attrs)
      assert html_response(conn, 200) =~ "New User"
    end
  end

  describe "edit user" do
    setup [:create_user]

    test "renders form for editing chosen user", %{conn: conn, user: user} do
      conn = get(conn, admin_user_path(conn, :edit, user))
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "update user" do
    setup [:create_user]

    test "redirects when data is valid", %{conn: conn, user: user} do
      conn = put(conn, admin_user_path(conn, :update, user), user: @update_attrs)
      assert redirected_to(conn) == admin_user_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put(conn, admin_user_path(conn, :update, user), user: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "delete user" do
    setup [:create_user]

    test "deletes chosen user", %{conn: conn, user: user} do
      response = delete(conn, admin_user_path(conn, :delete, user))
      assert redirected_to(response) == admin_user_path(response, :index)

      assert_error_sent(404, fn ->
        get(conn, admin_user_path(conn, :show, user))
      end)
    end
  end

  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end
end
