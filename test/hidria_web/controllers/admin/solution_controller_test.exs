defmodule HidriaWeb.Admin.SolutionControllerTest do
  use HidriaWeb.AdminConnCase
  import Hidria.Factory

  alias Hidria.Defsol

  @create_attrs %{description: "some description", title: "some title"}
  @update_attrs %{description: "some updated description", title: "some updated title"}
  @invalid_attrs %{description: nil, title: nil}

  def fixture(:solution, attrs \\ %{}) do
    cause = insert(:cause)

    {:ok, solution} =
      attrs
      |> Enum.into(%{cause_id: cause.id})
      |> Enum.into(%{steps: [%{title: "step1"}]})
      |> Enum.into(@create_attrs)
      |> Defsol.create_solution()

    solution
  end

  describe "index" do
    test "lists all solutions", %{conn: conn} do
      conn = get(conn, admin_solution_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Solutions"
    end
  end

  describe "new solution" do
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_solution_path(conn, :new))
      assert html_response(conn, 200) =~ "New Solution"
    end
  end

  describe "create solution" do
    test "redirects to show when data is valid", %{conn: conn} do
      params =
        %{cause_id: insert(:cause).id}
        |> Enum.into(%{steps: [%{title: "step1"}]})
        |> Enum.into(@create_attrs)

      conn = post(conn, admin_solution_path(conn, :create), solution: params)
      assert redirected_to(conn) == admin_solution_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_solution_path(conn, :create), solution: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Solution"
    end
  end

  describe "edit solution" do
    setup [:create_solution]

    test "renders form for editing chosen solution", %{conn: conn, solution: solution} do
      conn = get(conn, admin_solution_path(conn, :edit, solution))
      assert html_response(conn, 200) =~ "Edit Solution"
    end
  end

  describe "update solution" do
    setup [:create_solution]

    test "redirects when data is valid", %{conn: conn, solution: solution} do
      conn = put(conn, admin_solution_path(conn, :update, solution), solution: @update_attrs)
      assert redirected_to(conn) == admin_solution_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, solution: solution} do
      conn = put(conn, admin_solution_path(conn, :update, solution), solution: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Solution"
    end
  end

  describe "delete solution" do
    setup [:create_solution]

    test "deletes chosen solution", %{conn: conn, solution: solution} do
      conn = delete(conn, admin_solution_path(conn, :delete, solution))
      assert redirected_to(conn) == admin_solution_path(conn, :index)
    end
  end

  defp create_solution(_) do
    solution = fixture(:solution)
    {:ok, solution: solution}
  end
end
