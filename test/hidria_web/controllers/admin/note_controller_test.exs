defmodule HidriaWeb.Admin.NoteControllerTest do
  use HidriaWeb.AdminConnCase

  alias Hidria.Defsol
  alias Hidria.Fixtures

  @create_attrs %{description: "some description"}
  @update_attrs %{description: "some updated description"}
  @invalid_attrs %{description: nil}

  def user_fixture do
    Fixtures.user()
  end

  def fixture(:note) do
    params = %{inserted_by: user_fixture().id} |> Enum.into(@create_attrs)

    {:ok, note} = Defsol.create_note(params)

    note
  end

  describe "index" do
    test "lists all notes", %{conn: conn} do
      conn = get(conn, admin_note_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Notes"
    end
  end

  describe "new note" do
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_note_path(conn, :new))
      assert html_response(conn, 200) =~ "New Note"
    end
  end

  describe "create note" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, admin_note_path(conn, :create), note: @create_attrs)
      assert redirected_to(conn) == admin_note_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_note_path(conn, :create), note: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Note"
    end
  end

  describe "edit note" do
    setup [:create_note]

    test "renders form for editing chosen note", %{conn: conn, note: note} do
      conn = get(conn, admin_note_path(conn, :edit, note))
      assert html_response(conn, 200) =~ "Edit Note"
    end
  end

  describe "update note" do
    setup [:create_note]

    test "redirects when data is valid", %{conn: conn, note: note} do
      conn = put(conn, admin_note_path(conn, :update, note), note: @update_attrs)
      assert redirected_to(conn) == admin_note_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, note: note} do
      conn = put(conn, admin_note_path(conn, :update, note), note: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Note"
    end
  end

  describe "delete note" do
    setup [:create_note]

    test "deletes chosen note", %{conn: conn, note: note} do
      conn = delete(conn, admin_note_path(conn, :delete, note))
      assert redirected_to(conn) == admin_note_path(conn, :index)
    end
  end

  defp create_note(_) do
    note = fixture(:note)
    {:ok, note: note}
  end
end
