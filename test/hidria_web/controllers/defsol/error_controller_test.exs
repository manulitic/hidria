defmodule HidriaWeb.Defsol.ErrorControllerTest do
  use HidriaWeb.OperatorConnCase

  alias Hidria.Defsol

  @create_attrs %{title: "some title"}
  @invalid_attrs %{title: nil}

  def fixture(:error) do
    {:ok, error} = Defsol.create_error(@create_attrs)
    error
  end

  describe "index" do
    test "lists all errors", %{conn: conn} do
      conn = get(conn, defsol_error_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Errors"
    end
  end

  describe "new error" do
    test "renders form", %{conn: conn} do
      conn = get(conn, defsol_error_path(conn, :new))
      assert html_response(conn, 200) =~ "New Error"
    end
  end

  describe "create error" do
    test "redirects to attachments after error creation", %{conn: conn} do
      conn = post(conn, defsol_error_path(conn, :create), error: @create_attrs)
      redirected_to = conn |> redirected_to() |> String.split("?") |> hd()
      assert redirected_to == attachment_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, defsol_error_path(conn, :create), error: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Error"
    end
  end

  describe "edit error" do
    setup [:create_error]

    test "renders form for editing chosen error", %{conn: conn, error: error} do
      conn = get(conn, defsol_error_path(conn, :edit, error))
      assert html_response(conn, 200) =~ "Edit Error"
    end
  end

  describe "update error" do
    setup [:create_error]

    test "redirects back when data is invalid", %{conn: conn, error: error} do
      path = defsol_error_path(conn, :index)
      params = @invalid_attrs |> Enum.into(%{"redirect_to" => path})
      conn = put(conn, defsol_error_path(conn, :update, error), error: params)
      assert redirected_to(conn) == path
    end
  end

  defp create_error(_) do
    error = fixture(:error)
    {:ok, error: error}
  end
end
