defmodule Hidria.ShopfloorTest do
  use Hidria.DataCase
  import Hidria.Factory

  alias Hidria.Shopfloor

  describe "parts" do
    alias Hidria.Shopfloor.Part

    @valid_attrs %{code: "some code", description: "some description"}
    @update_attrs %{code: "some updated code", description: "some updated description"}
    @invalid_attrs %{code: nil, description: nil}

    def part_fixture(attrs \\ %{}) do
      {:ok, part} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Shopfloor.create_part()

      part
    end

    test "list_parts/0 returns all parts" do
      part = part_fixture()
      [result] = Shopfloor.list_parts()
      assert result.id == part.id
    end

    test "get_part!/1 returns the part with given id" do
      part = part_fixture()
      assert Shopfloor.get_part!(part.id) == part
    end

    test "create_part/1 with valid data creates a part" do
      assert {:ok, %Part{} = part} = Shopfloor.create_part(@valid_attrs)
      assert part.code == "some code"
      assert part.description == "some description"
    end

    test "create_part/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Shopfloor.create_part(@invalid_attrs)
    end

    test "update_part/2 with valid data updates the part" do
      part = part_fixture()
      assert {:ok, part} = Shopfloor.update_part(part, @update_attrs)
      assert %Part{} = part
      assert part.code == "some updated code"
      assert part.description == "some updated description"
    end

    test "update_part/2 with invalid data returns error changeset" do
      part = part_fixture()
      assert {:error, %Ecto.Changeset{}} = Shopfloor.update_part(part, @invalid_attrs)
      assert part == Shopfloor.get_part!(part.id)
    end

    test "delete_part/1 deletes the part" do
      part = part_fixture()
      assert {:ok, %Part{}} = Shopfloor.delete_part(part)
      assert_raise Ecto.NoResultsError, fn -> Shopfloor.get_part!(part.id) end
    end

    test "change_part/1 returns a part changeset" do
      part = part_fixture()
      assert %Ecto.Changeset{} = Shopfloor.change_part(part)
    end
  end

  describe "stations" do
    alias Hidria.Shopfloor.Station

    @valid_attrs %{code: "some code", description: "some description"}
    @update_attrs %{code: "some updated code", description: "some updated description"}
    @invalid_attrs %{code: nil, description: nil}

    def station_fixture(attrs \\ %{}) do
      {:ok, station} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Shopfloor.create_station()

      station
    end

    test "list_stations/0 returns all stations" do
      station = station_fixture()
      [result] = Shopfloor.list_stations()
      assert result.id == station.id
    end

    test "get_station!/1 returns the station with given id" do
      station = station_fixture()
      assert Shopfloor.get_station!(station.id) == station
    end

    test "create_station/1 with valid data creates a station" do
      assert {:ok, %Station{} = station} = Shopfloor.create_station(@valid_attrs)
      assert station.code == "some code"
      assert station.description == "some description"
    end

    test "create_station/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Shopfloor.create_station(@invalid_attrs)
    end

    test "update_station/2 with valid data updates the station" do
      station = station_fixture()
      assert {:ok, station} = Shopfloor.update_station(station, @update_attrs)
      assert %Station{} = station
      assert station.code == "some updated code"
      assert station.description == "some updated description"
    end

    test "update_station/2 with invalid data returns error changeset" do
      station = station_fixture()
      assert {:error, %Ecto.Changeset{}} = Shopfloor.update_station(station, @invalid_attrs)
      assert station == Shopfloor.get_station!(station.id)
    end

    test "delete_station/1 deletes the station" do
      station = station_fixture()
      assert {:ok, %Station{}} = Shopfloor.delete_station(station)
      assert_raise Ecto.NoResultsError, fn -> Shopfloor.get_station!(station.id) end
    end

    test "change_station/1 returns a station changeset" do
      station = station_fixture()
      assert %Ecto.Changeset{} = Shopfloor.change_station(station)
    end
  end

  describe "stations many_to_many parts" do
    @station_attrs %{code: "some code", description: "some description"}
    @part_attrs %{code: "some code", description: "some description"}

    test "create_station/1 adds the parts" do
      parts = insert_list(3, :part) |> Enum.map(fn p -> p.id end)

      {:ok, station} =
        @station_attrs
        |> Enum.into(%{parts: parts})
        |> Shopfloor.create_station()

      station = Shopfloor.get_station!(station.id)
      assert Enum.map(station.parts, fn p -> p.id end) == parts
    end

    test "update_station/2 adds the parts" do
      station = insert(:station)
      parts = insert_list(3, :part) |> Enum.map(fn p -> p.id end)

      Shopfloor.update_station(station, %{parts: parts})
      station = Shopfloor.get_station!(station.id)
      assert Enum.map(station.parts, fn p -> p.id end) == parts
    end

    test "update_station/2 removes the parts" do
      parts1 = insert_list(3, :part) |> Enum.map(fn p -> p.id end)

      {:ok, station} =
        @station_attrs
        |> Enum.into(%{parts: parts1})
        |> Shopfloor.create_station()

      parts2 = insert_list(2, :part) |> Enum.map(fn p -> p.id end)
      Shopfloor.update_station(station, %{parts: parts2})
      station = Shopfloor.get_station!(station.id)
      assert Enum.map(station.parts, fn p -> p.id end) == parts2
    end

    test "removing a part removes its association to station" do
      parts = insert_list(3, :part) |> Enum.map(fn p -> p.id end)

      {:ok, station} =
        @station_attrs
        |> Enum.into(%{parts: parts})
        |> Shopfloor.create_station()

      [head | parts_tail] = parts
      head |> Shopfloor.get_part!() |> Shopfloor.delete_part()

      station = Shopfloor.get_station!(station.id)
      assert Enum.map(station.parts, fn p -> p.id end) == parts_tail
    end

    test "create_part/1 adds the stations" do
      stations = insert_list(3, :station) |> Enum.map(fn p -> p.id end)

      {:ok, part} =
        @part_attrs
        |> Enum.into(%{stations: stations})
        |> Shopfloor.create_part()

      part = Shopfloor.get_part!(part.id)
      assert Enum.map(part.stations, fn p -> p.id end) == stations
    end

    test "update_part/2 adds the parts" do
      part = insert(:part)
      stations = insert_list(3, :station) |> Enum.map(fn p -> p.id end)

      Shopfloor.update_part(part, %{stations: stations})
      part = Shopfloor.get_part!(part.id)
      assert Enum.map(part.stations, fn p -> p.id end) == stations
    end

    test "update_part/2 removes the parts" do
      stations = insert_list(3, :station) |> Enum.map(fn p -> p.id end)

      {:ok, part} =
        @part_attrs
        |> Enum.into(%{stations: stations})
        |> Shopfloor.create_part()

      stations2 = insert_list(2, :station) |> Enum.map(fn p -> p.id end)
      Shopfloor.update_part(part, %{stations: stations2})
      part = Shopfloor.get_part!(part.id)
      assert Enum.map(part.stations, fn p -> p.id end) == stations2
    end

    test "removing a station removes its association from part" do
      stations = insert_list(3, :station) |> Enum.map(fn p -> p.id end)

      {:ok, part} =
        @part_attrs
        |> Enum.into(%{stations: stations})
        |> Shopfloor.create_part()

      [head | stations_tail] = stations
      head |> Shopfloor.get_station!() |> Shopfloor.delete_station()

      part = Shopfloor.get_part!(part.id)
      assert Enum.map(part.stations, fn p -> p.id end) == stations_tail
    end

    test "list_parts/1 filters by station_id" do
      [station1, station2] = insert_list(2, :station) |> Enum.map(fn p -> p.id end)

      insert_list(2, :part) |> Enum.map(&Shopfloor.update_part(&1, %{stations: [station1]}))
      insert_list(3, :part) |> Enum.map(&Shopfloor.update_part(&1, %{stations: [station2]}))

      insert_list(1, :part)
      |> Enum.map(&Shopfloor.update_part(&1, %{stations: [station1, station2]}))

      assert Shopfloor.list_parts(%{station_id: station1}) |> page_size() == 3
      assert Shopfloor.list_parts(%{station_id: station2}) |> page_size() == 4
      assert Shopfloor.list_parts() |> page_size() == 6
    end

    test "list_stations/1 filters by part_id" do
      [part1, part2] = insert_list(2, :part) |> Enum.map(fn p -> p.id end)

      insert_list(2, :station) |> Enum.map(&Shopfloor.update_station(&1, %{parts: [part1]}))
      insert_list(3, :station) |> Enum.map(&Shopfloor.update_station(&1, %{parts: [part2]}))

      insert_list(1, :station)
      |> Enum.map(&Shopfloor.update_station(&1, %{parts: [part1, part2]}))

      assert Shopfloor.list_stations(%{part_id: part1}) |> page_size() == 3
      assert Shopfloor.list_stations(%{part_id: part2}) |> page_size() == 4
      assert Shopfloor.list_stations() |> page_size() == 6
    end
  end
end
