defmodule Hidria.CloneTest do
  use Hidria.DataCase
  import Hidria.Factory

  alias Hidria.Defsol
  alias Hidria.Repo

  describe "Clone protocol implementation for Attachment" do
    @valid_attrs %{
      fileinfo: %Plug.Upload{path: "test/attachment_files/example.png", filename: "example.png"}
    }
    @other_attrs %{
      fileinfo: %Plug.Upload{path: "test/attachment_files/example2.png", filename: "example.png"}
    }

    def attachment_fixture(attrs \\ %{}) do
      {:ok, attachment} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Defsol.create_attachment()

      attachment
    end

    test "works without parent" do
      attachment = attachment_fixture()
      clone = Clone.clone(attachment)

      assert attachment.id != clone.id
      assert attachment.inserted_at != clone.inserted_at
      assert attachment.updated_at != clone.updated_at
      assert attachment.filehash == clone.filehash
      assert attachment.fileinfo == clone.fileinfo
    end

    test "is_clone?/2" do
      attachment = attachment_fixture()
      not_a_clone = attachment_fixture(@other_attrs)
      clone = Clone.clone(attachment)

      assert Clone.is_clone?(attachment, clone)
      assert Clone.is_clone?(clone, attachment)
      refute Clone.is_clone?(not_a_clone, attachment)
      refute Clone.is_clone?(attachment, not_a_clone)
      refute Clone.is_clone?(not_a_clone, clone)
      refute Clone.is_clone?(clone, not_a_clone)
    end

    test "works with parent" do
      error = insert(:error)
      attachment = attachment_fixture(%{error_id: error.id})
      clone = Clone.clone(attachment)

      assert attachment.id != clone.id
      assert attachment.error_id == clone.error_id
    end

    test "works changing the parent" do
      error = insert(:error)
      clone_error = insert(:error)
      attachment = attachment_fixture(%{error_id: error.id})
      clone = Clone.clone(attachment, %{error_id: clone_error.id})

      assert attachment.id != clone.id
      assert attachment.error_id != clone.error_id
    end
  end

  describe "Clone protocol implementation for SolutionStep" do
    @valid_attrs %{
      title: "step"
    }
    @other_attrs %{
      title: "something else"
    }

    def step_fixture(attrs \\ %{}) do
      {:ok, step} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Defsol.create_solution_step()

      step
    end

    test "without attachments" do
      step = step_fixture()
      clone = Clone.clone(step)

      assert step.id != clone.id
      assert step.title == clone.title
    end

    test "without attachments different solution" do
      [solution, solution_clone] = insert_list(2, :solution)
      step = step_fixture(%{solution_id: solution.id})
      clone = Clone.clone(step, %{solution_id: solution_clone.id})

      assert step.id != clone.id
      assert step.solution_id != clone.solution_id
      assert step.title == clone.title
    end

    test "with attachments" do
      step = step_fixture()
      attachment_fixture(%{solution_step_id: step.id})
      step = step |> Repo.preload(:attachments)

      clone = Clone.clone(step) |> Repo.preload(:attachments)
      assert step.id != clone.id

      Enum.each(step.attachments, fn a ->
        attachment_clones = Enum.filter(clone.attachments, fn ca -> Clone.is_clone?(a, ca) end)
        assert length(attachment_clones) == 1
      end)
    end

    test "is_clone?/2" do
      step = step_fixture()
      not_a_clone = step_fixture(@other_attrs)
      clone = Clone.clone(step)

      assert Clone.is_clone?(step, clone)
      assert Clone.is_clone?(clone, step)
      refute Clone.is_clone?(not_a_clone, step)
      refute Clone.is_clone?(step, not_a_clone)
      refute Clone.is_clone?(not_a_clone, clone)
      refute Clone.is_clone?(clone, not_a_clone)
    end
  end

  describe "Clone protocol implementation for Solution" do
    test "clone/2" do
      solution = insert(:solution)
      clone = Clone.clone(solution)

      assert solution.id != clone.id
      assert solution.title == clone.title
      assert solution.description == clone.description
    end

    test "clone/2 with different cause" do
      [cause, cause_two] = insert_list(2, :cause)
      solution = insert(:solution, cause_id: cause.id)
      clone = Clone.clone(solution, %{cause_id: cause_two.id})

      assert solution.id != clone.id
      assert solution.cause_id != clone.cause_id
      assert solution.title == clone.title
      assert solution.description == clone.description
    end

    test "clone/2 with steps" do
      solution = insert(:solution, steps: [%{title: "one"}, %{title: "two"}])
      clone = Clone.clone(solution) |> Defsol.preload_steps()
      solution = solution |> Repo.preload(:steps)

      assert solution.id != clone.id

      # check that the steps are in the same order
      solution.steps
      |> Enum.with_index()
      |> Enum.each(fn {step, i} ->
        clone_step = Enum.at(clone.steps, i)
        assert Clone.is_clone?(step, clone_step)
      end)
    end

    test "is_clone?/2" do
      [item, not_a_clone] = insert_list(2, :solution)
      clone = Clone.clone(item)

      assert Clone.is_clone?(item, clone)
      assert Clone.is_clone?(clone, item)
      refute Clone.is_clone?(not_a_clone, item)
      refute Clone.is_clone?(item, not_a_clone)
      refute Clone.is_clone?(not_a_clone, clone)
      refute Clone.is_clone?(clone, not_a_clone)
    end
  end

  describe "Clone protocol implementation for Cause" do
    test "clone/2" do
      cause = insert(:cause)
      clone = Clone.clone(cause)

      assert cause.id != clone.id
      assert cause.title == clone.title
      assert cause.description == clone.description
    end

    test "clone/2 with different error" do
      [error, error_two] = insert_list(2, :error)
      cause = insert(:cause, error_id: error.id)
      clone = Clone.clone(cause, %{error_id: error_two.id})

      assert cause.id != clone.id
      assert cause.error_id != clone.error_id
      assert cause.title == clone.title
      assert cause.description == clone.description
    end

    test "clone/2 with solutions" do
      cause = insert(:cause)
      insert_list(2, :solution, cause_id: cause.id)
      clone = Clone.clone(cause) |> Repo.preload(:solutions)
      cause = cause |> Repo.preload(:solutions)

      assert cause.id != clone.id

      cause.solutions
      |> Enum.each(fn x ->
        solution_clones = Enum.filter(clone.solutions, fn y -> Clone.is_clone?(x, y) end)
        assert length(solution_clones) == 1
      end)
    end

    test "is_clone?/2" do
      [item, not_a_clone] = insert_list(2, :cause)
      clone = Clone.clone(item)

      assert Clone.is_clone?(item, clone)
      assert Clone.is_clone?(clone, item)
      refute Clone.is_clone?(not_a_clone, item)
      refute Clone.is_clone?(item, not_a_clone)
      refute Clone.is_clone?(not_a_clone, clone)
      refute Clone.is_clone?(clone, not_a_clone)
    end
  end

  describe "Clone protocol implementation for Error" do
    test "clone/2" do
      error = insert(:error)
      clone = Clone.clone(error)

      assert error.id != clone.id
      assert error.title == clone.title
      assert error.description == clone.description
      assert error.active == clone.active
    end

    test "clone/2 with different station and part" do
      [station, station_two] = insert_list(2, :station)
      [part, part_two] = insert_list(2, :part)
      error = insert(:error, station_id: station.id, part_id: part.id)
      clone = Clone.clone(error, %{station_id: station_two.id, part_id: part_two.id})

      assert error.id != clone.id
      assert error.station_id != clone.station_id
      assert error.part_id != clone.part_id
      assert clone.station_id == station_two.id
      assert clone.part_id == part_two.id
    end

    test "clone/2 with causes" do
      error = insert(:error)
      insert_list(2, :cause, error_id: error.id)
      clone = Clone.clone(error) |> Repo.preload(:causes)
      error = error |> Repo.preload(:causes)

      assert error.id != clone.id

      error.causes
      |> Enum.each(fn x ->
        cause_clones = Enum.filter(clone.causes, fn y -> Clone.is_clone?(x, y) end)
        assert length(cause_clones) == 1
      end)
    end

    test "is_clone?/2" do
      [item, not_a_clone] = insert_list(2, :error)
      clone = Clone.clone(item)

      assert Clone.is_clone?(item, clone)
      assert Clone.is_clone?(clone, item)
      refute Clone.is_clone?(not_a_clone, item)
      refute Clone.is_clone?(item, not_a_clone)
      refute Clone.is_clone?(not_a_clone, clone)
      refute Clone.is_clone?(clone, not_a_clone)
    end
  end
end
