defmodule Hidria.AccountsTest do
  use Hidria.DataCase

  alias Hidria.Accounts

  describe "users" do
    alias Hidria.Accounts.User

    @valid_attrs %{
      fullname: "some fullname",
      password_confirmation: "somepassword_hash",
      password: "somepassword_hash",
      role: "operator",
      username: "some username"
    }
    @update_attrs %{
      fullname: "some updated fullname",
      role: "operator",
      username: "some updated username"
    }
    @invalid_attrs %{fullname: nil, password: nil, role: nil, username: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.fullname == "some fullname"
      assert user.role == "operator"
      assert user.username == "some username"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Accounts.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.fullname == "some updated fullname"
      assert user.username == "some updated username"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "delete_user/1 won't delete the last admin" do
      admin1 = user_fixture(%{role: "admin", username: "admin1"})
      admin2 = user_fixture(%{role: "admin", username: "admin2"})

      assert {:ok, %User{}} = Accounts.delete_user(admin1)
      assert {:error, %Ecto.Changeset{}} = Accounts.delete_user(admin2)
      assert admin2 == Accounts.get_user!(admin2.id)
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end

    def user_create(attrs = %{role: role}) do
      attrs
      |> Enum.into(%{username: role})
      |> Enum.into(@valid_attrs)
      |> Accounts.create_user()
    end

    test "are admin, lead and operator" do
      assert {:ok, _} = user_create(%{role: "admin"})
      assert {:ok, _} = user_create(%{role: "lead"})
      assert {:ok, _} = user_create(%{role: "operator"})
      assert {:error, _} = user_create(%{role: "wrong"})
      assert {:error, _} = user_create(%{role: ""})
    end
  end
end
