defmodule Hidria.DefsolTest do
  use Hidria.DataCase
  import Hidria.Factory

  alias Hidria.Defsol
  alias Hidria.Fixtures

  def user_fixture do
    Fixtures.user()
  end

  describe "errors" do
    alias Hidria.Defsol.Error

    @valid_attrs %{title: "some title", description: "some description"}
    @update_attrs %{title: "some updated title"}
    @invalid_attrs %{title: nil}

    def error_fixture(attrs \\ %{}) do
      {:ok, error} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Defsol.create_error()

      error
    end

    def build_parents do
      station = insert(:station)
      part1 = insert(:part)
      part2 = insert(:part)

      {[station], [part1, part2]}
    end

    test "list_errors/0 returns all errors" do
      error = error_fixture()
      ids = Defsol.list_errors() |> Enum.map(fn x -> Map.get(x, :id) end)
      assert ids == [error.id]
    end

    test "list_errors/1 accepts search, part and station params" do
      title = "asdf"

      {[station | _], [part1, part2 | _]} = build_parents()
      insert(:error, part_id: part1.id, station_id: station.id)
      insert(:error, part_id: part2.id, station_id: station.id, title: title)
      insert(:error, part_id: part2.id, station_id: station.id)

      assert Defsol.list_errors(%{"part_id" => part1.id}) |> page_size() == 1
      assert Defsol.list_errors(%{"part_id" => part2.id}) |> page_size() == 2
      assert Defsol.list_errors(%{"station_id" => station.id}) |> page_size() == 3

      assert Defsol.list_errors(%{"part_id" => part2.id, "station_id" => station.id})
             |> page_size() == 2

      assert Defsol.list_errors(%{
               "part_id" => part2.id,
               "station_id" => station.id,
               "search" => title
             })
             |> page_size() == 1
    end

    test "list_active_errors/1 uses params" do
      {[station | _], [part1, part2 | _]} = build_parents()
      insert(:error, part_id: part1.id, station_id: station.id)
      insert(:error, part_id: part2.id, station_id: station.id)
      insert(:error, part_id: part2.id, station_id: station.id, active: true)

      active = Defsol.list_active_errors()
      assert page_size(active) == 1

      active = Defsol.list_active_errors(%{part_id: part1.id})
      assert page_size(active) == 0
    end

    test "get_error!/1 returns the error with given id" do
      error = error_fixture()
      result = Defsol.get_error!(error.id)

      [:id, :title]
      |> Enum.each(fn key ->
        assert Map.get(result, key) == Map.get(error, key)
      end)
    end

    test "create_error/1 with valid data creates a error" do
      assert {:ok, %Error{} = error} = Defsol.create_error(@valid_attrs)
      assert error.title == "some title"
    end

    test "create_error/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Defsol.create_error(@invalid_attrs)
    end

    test "update_error/2 with valid data updates the error" do
      error = error_fixture()
      assert {:ok, error} = Defsol.update_error(error, @update_attrs)
      assert %Error{} = error
      assert error.title == "some updated title"
    end

    test "update_error/2 with invalid data returns error changeset" do
      error = error_fixture()
      assert {:error, %Ecto.Changeset{}} = Defsol.update_error(error, @invalid_attrs)
    end

    test "delete_error/1 deletes the error" do
      error = error_fixture()
      assert {:ok, %Error{}} = Defsol.delete_error(error)
      assert_raise Ecto.NoResultsError, fn -> Defsol.get_error!(error.id) end
    end

    test "change_error/1 returns a error changeset" do
      error = error_fixture()
      assert %Ecto.Changeset{} = Defsol.change_error(error)
    end
  end

  describe "causes" do
    alias Hidria.Defsol.Cause

    @valid_attrs %{
      active: true,
      description: "some description",
      detection: "some detection",
      title: "some title"
    }
    @update_attrs %{
      active: false,
      description: "some updated description",
      detection: "some updated detection",
      title: "some updated title"
    }
    @invalid_attrs %{active: nil, description: nil, detection: nil, title: nil}

    def cause_fixture(attrs \\ %{}) do
      error = insert(:error)

      {:ok, cause} =
        attrs
        |> Enum.into(%{error_id: error.id})
        |> Enum.into(@valid_attrs)
        |> Defsol.create_cause()

      cause
    end

    test "list_causes/0 returns all causes" do
      cause = cause_fixture()
      assert Defsol.list_causes() == [cause]
    end

    test "get_cause!/1 returns the cause with given id" do
      cause = cause_fixture()
      result = Defsol.get_cause!(cause.id)
      assert cause.id == result.id
      assert cause.active == result.active
      assert cause.description == result.description
      assert cause.detection == result.detection
      assert cause.title == result.title
    end

    test "create_cause/1 with valid data creates a cause" do
      params = %{error_id: insert(:error).id} |> Enum.into(@valid_attrs)
      assert {:ok, %Cause{} = cause} = Defsol.create_cause(params)
      assert cause.active == true
      assert cause.description == "some description"
      assert cause.detection == "some detection"
      assert cause.title == "some title"
    end

    test "create_cause/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Defsol.create_cause(@invalid_attrs)
    end

    test "update_cause/2 with valid data updates the cause" do
      cause = cause_fixture()
      assert {:ok, cause} = Defsol.update_cause(cause, @update_attrs)
      assert %Cause{} = cause
      assert cause.active == false
      assert cause.description == "some updated description"
      assert cause.detection == "some updated detection"
      assert cause.title == "some updated title"
    end

    test "update_cause/2 sets the error to active if cause is activated" do
      cause = cause_fixture(active: false) |> Hidria.Repo.preload(:error)

      assert cause.active == false
      assert cause.error.active == false

      assert {:ok, cause} = Defsol.update_cause(cause, %{active: true})
      cause = cause |> Hidria.Repo.preload(:error)
      assert cause.active == true
      assert cause.error.active == true
    end

    test "update_cause/2 with invalid data returns error changeset" do
      cause = cause_fixture()
      assert {:error, %Ecto.Changeset{}} = Defsol.update_cause(cause, @invalid_attrs)
      result = Defsol.get_cause!(cause.id)
      assert cause.id == result.id
      assert cause.active == result.active
      assert cause.description == result.description
      assert cause.detection == result.detection
      assert cause.title == result.title
    end

    test "delete_cause/1 deletes the cause" do
      cause = cause_fixture()
      assert {:ok, %Cause{}} = Defsol.delete_cause(cause)
      assert_raise Ecto.NoResultsError, fn -> Defsol.get_cause!(cause.id) end
    end

    test "change_cause/1 returns a cause changeset" do
      cause = cause_fixture()
      assert %Ecto.Changeset{} = Defsol.change_cause(cause)
    end
  end

  describe "solutions" do
    alias Hidria.Defsol.Solution

    @valid_attrs %{description: "some description", title: "some title"}
    @update_attrs %{description: "some updated description", title: "some updated title"}
    @invalid_attrs %{description: nil, title: nil}

    def solution_fixture(attrs \\ %{}) do
      cause = insert(:cause)

      {:ok, solution} =
        attrs
        |> Enum.into(%{cause_id: cause.id})
        |> Enum.into(%{steps: [%{title: "step1"}]})
        |> Enum.into(@valid_attrs)
        |> Defsol.create_solution()

      solution
    end

    test "list_solutions/0 returns all solutions" do
      solution = solution_fixture()
      [result] = Defsol.list_solutions()
      assert solution.id == result.id
      assert solution.title == result.title
      assert solution.description == result.description
    end

    test "get_solution!/1 returns the solution with given id" do
      solution = solution_fixture()
      result = Defsol.get_solution!(solution.id)
      assert solution.id == result.id
      assert solution.title == result.title
      assert solution.description == result.description
    end

    test "create_solution/1 with valid data creates a solution" do
      params =
        %{cause_id: insert(:cause).id, steps: [%{title: "step1"}]}
        |> Enum.into(@valid_attrs)

      assert {:ok, %Solution{} = solution} = Defsol.create_solution(params)
      assert solution.description == "some description"
      assert solution.title == "some title"
    end

    test "create_solution/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Defsol.create_solution(@invalid_attrs)
    end

    test "update_solution/2 with valid data updates the solution" do
      solution = solution_fixture()
      assert {:ok, solution} = Defsol.update_solution(solution, @update_attrs)
      assert %Solution{} = solution
      assert solution.description == "some updated description"
      assert solution.title == "some updated title"
    end

    test "update_solution/2 with invalid data returns error changeset" do
      solution = solution_fixture()
      assert {:error, %Ecto.Changeset{}} = Defsol.update_solution(solution, @invalid_attrs)

      result = Defsol.get_solution!(solution.id)
      assert solution.id == result.id
      assert solution.title == result.title
      assert solution.description == result.description
    end

    test "delete_solution/1 deletes the solution" do
      solution = solution_fixture()
      solution = Defsol.get_solution!(solution.id)
      assert {:ok, %Solution{}} = Defsol.delete_solution(solution)
      assert_raise Ecto.NoResultsError, fn -> Defsol.get_solution!(solution.id) end
    end

    test "change_solution/1 returns a solution changeset" do
      solution = solution_fixture()
      assert %Ecto.Changeset{} = Defsol.change_solution(solution)
    end
  end

  describe "solutions steps" do
    alias Hidria.Defsol.Solution
    alias Hidria.Defsol.SolutionStep

    test "new_solution/1 has one empty step" do
      solution = Defsol.new_solution() |> Ecto.Changeset.apply_changes()
      steps = solution.steps

      assert length(steps) == 1
      assert [%SolutionStep{}] = steps
    end

    test "change_solution/2 casts solution steps" do
      attrs = %{title: "hello", steps: [%{title: "one"}]}
      solution = Defsol.change_solution(%Solution{}, attrs) |> Ecto.Changeset.apply_changes()

      assert [step] = solution.steps
      assert step.title == "one"
    end

    test "add a new step to existing solution, removes the old one" do
      params =
        %{cause_id: insert(:cause).id, steps: [%{title: "step1"}]}
        |> Enum.into(@valid_attrs)

      assert {:ok, %Solution{} = solution} = Defsol.create_solution(params)

      steps = [%{title: "new step"}]

      {:ok, solution} =
        Defsol.change_solution(solution, %{steps: steps})
        |> Repo.update()

      assert [step] = solution.steps
      assert step.title == "new step"
    end

    test "add a new step, keep the existing one" do
      params =
        %{cause_id: insert(:cause).id, steps: [%{title: "step1"}]}
        |> Enum.into(@valid_attrs)

      assert {:ok, %Solution{} = solution} = Defsol.create_solution(params)

      {:ok, solution} =
        Defsol.add_step_to_solution(solution, %{title: "step2"})
        |> Repo.update()

      assert [step1, step2] = solution.steps
      assert step1.title == "step1"
      assert step2.title == "step2"
    end
  end

  describe "reports" do
    alias Hidria.Defsol.Report

    @valid_attrs %{description: "some description", success: true}
    @update_attrs %{description: "some updated description", success: false}
    @invalid_attrs %{description: nil, success: nil}

    def report_fixture(attrs \\ %{}) do
      solution = insert(:solution)

      {:ok, report} =
        attrs
        |> Enum.into(%{solution_id: solution.id})
        |> Enum.into(%{inserted_by: user_fixture().id})
        |> Enum.into(@valid_attrs)
        |> Defsol.create_report()

      Defsol.get_report!(report.id)
    end

    test "list_reports/0 returns all reports" do
      inserted = report_fixture()
      [report] = Defsol.list_reports()
      assert report.id == inserted.id
      assert report.description == inserted.description
      assert report.solution_id == inserted.solution_id
      assert report.inserted_by == inserted.inserted_by
    end

    test "get_report!/1 returns the report with given id" do
      report = report_fixture()
      assert Defsol.get_report!(report.id) == report
    end

    test "create_report/1 with valid data creates a report" do
      params =
        %{solution_id: insert(:solution).id, inserted_by: user_fixture().id}
        |> Enum.into(@valid_attrs)

      assert {:ok, %Report{} = report} = Defsol.create_report(params)
      assert report.description == "some description"
      assert report.success == true
    end

    test "create_report/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Defsol.create_report(@invalid_attrs)
    end

    test "update_report/2 with valid data updates the report" do
      report = report_fixture()
      assert {:ok, report} = Defsol.update_report(report, @update_attrs)
      assert %Report{} = report
      assert report.description == "some updated description"
      assert report.success == false
    end

    test "update_report/2 with invalid data returns error changeset" do
      report = report_fixture()
      assert {:error, %Ecto.Changeset{}} = Defsol.update_report(report, @invalid_attrs)
      assert report == Defsol.get_report!(report.id)
    end

    test "delete_report/1 deletes the report" do
      report = report_fixture()
      assert {:ok, %Report{}} = Defsol.delete_report(report)
      assert_raise Ecto.NoResultsError, fn -> Defsol.get_report!(report.id) end
    end

    test "change_report/1 returns a report changeset" do
      report = report_fixture()
      assert %Ecto.Changeset{} = Defsol.change_report(report)
    end

    test "update_active/1 updated active field on cause and error" do
      error = insert(:error, active: true)
      cause = insert(:cause, error_id: error.id, active: true)
      solution = insert(:solution, cause_id: cause.id)

      report = report_fixture(%{solution_id: solution.id})

      assert error.active
      assert cause.active

      Defsol.update_active(report)
      error = Defsol.get_error!(error.id)
      cause = Defsol.get_cause!(cause.id)

      refute error.active
      refute cause.active
    end
  end

  describe "notes" do
    alias Hidria.Defsol.Note

    @valid_attrs %{description: "some description"}
    @update_attrs %{description: "some updated description"}
    @invalid_attrs %{description: nil}

    def note_fixture(attrs \\ %{}) do
      {:ok, note} =
        attrs
        |> Enum.into(%{inserted_by: user_fixture().id})
        |> Enum.into(@valid_attrs)
        |> Defsol.create_note()

      note
    end

    test "list_notes/0 returns all notes" do
      note = note_fixture()
      [result] = Defsol.list_notes()
      assert note.id == result.id
      assert note.description == result.description
    end

    test "list_notes/1 lists the notes with correct association" do
      user = user_fixture()
      error = insert(:error)
      cause = insert(:cause)
      solution = insert(:solution)

      insert(:note, author: user, error: error)
      insert(:note, author: user, error: insert(:error))
      insert_list(2, :note, author: user, cause: cause)
      insert_list(3, :note, author: user, solution: solution)

      assert Defsol.list_notes(%{error_id: error.id}) |> page_size() == 1
      assert Defsol.list_notes(%{"error_id" => error.id}) |> page_size() == 1
      assert Defsol.list_notes(%{cause_id: cause.id}) |> page_size() == 2
      assert Defsol.list_notes(%{"cause_id" => cause.id}) |> page_size() == 2
      assert Defsol.list_notes(%{solution_id: solution.id}) |> page_size() == 3
      assert Defsol.list_notes(%{"solution_id" => solution.id}) |> page_size() == 3
    end

    test "list_notes/1 by associations group" do
      user = user_fixture()
      error = insert(:error)
      cause = insert(:cause)
      solution = insert(:solution)

      insert(:note, author: user, error: error)
      insert(:note, author: user, error: insert(:error))
      insert_list(2, :note, author: user, cause: cause)
      insert_list(3, :note, author: user, solution: solution)

      assert Defsol.list_notes(%{association: "errors"}) |> page_size() == 2
      assert Defsol.list_notes(%{"association" => "errors"}) |> page_size() == 2
      assert Defsol.list_notes(%{association: "causes"}) |> page_size() == 2
      assert Defsol.list_notes(%{"association" => "causes"}) |> page_size() == 2
      assert Defsol.list_notes(%{association: "solutions"}) |> page_size() == 3
      assert Defsol.list_notes(%{"association" => "solutions"}) |> page_size() == 3
    end

    test "get_note!/1 returns the note with given id" do
      note = note_fixture()
      result = Defsol.get_note!(note.id)
      assert note.id == result.id
      assert note.description == result.description
    end

    test "create_note/1 with valid data creates a note" do
      user = user_fixture()
      params = %{inserted_by: user.id} |> Enum.into(@valid_attrs)
      assert {:ok, %Note{} = note} = Defsol.create_note(params)
      assert note.description == "some description"
      assert note.inserted_by == user.id
    end

    test "create_note/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Defsol.create_note(@invalid_attrs)
    end

    test "create_note/1 with error" do
      error = error_fixture()

      params =
        %{inserted_by: user_fixture().id, error_id: error.id}
        |> Enum.into(@valid_attrs)

      assert {:ok, %Note{} = note} = Defsol.create_note(params)

      note = note |> Repo.preload(:error)
      assert note.error.id == error.id
    end

    test "update_note/2 with valid data updates the note" do
      note = note_fixture()
      assert {:ok, note} = Defsol.update_note(note, @update_attrs)
      assert %Note{} = note
      assert note.description == "some updated description"
    end

    test "update_note/2 with invalid data returns error changeset" do
      note = note_fixture()
      assert {:error, %Ecto.Changeset{}} = Defsol.update_note(note, @invalid_attrs)
      result = Defsol.get_note!(note.id)
      assert note.id == result.id
      assert note.description == result.description
    end

    test "delete_note/1 deletes the note" do
      note = note_fixture()
      assert {:ok, %Note{}} = Defsol.delete_note(note)
      assert_raise Ecto.NoResultsError, fn -> Defsol.get_note!(note.id) end
    end

    test "change_note/1 returns a note changeset" do
      note = note_fixture()
      assert %Ecto.Changeset{} = Defsol.change_note(note)
    end
  end
end
