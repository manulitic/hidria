defmodule Hidria.ReportTest do
  use Hidria.DataCase
  import Hidria.Factory

  alias Hidria.Defsol

  describe "association with station and part" do
    test "list_reports_by_shopfloor/1 lists reports by station and part id" do
      [sol1, sol2] = insert_list(2, :solution)

      [report1 | _] = insert_list(2, :report, solution: sol1)
      [report2 | _] = insert_list(3, :report, solution: sol2)

      {station1_id, part1_id} = extract_station_error(report1)
      {station2_id, part2_id} = extract_station_error(report2)

      assert Defsol.list_reports_by_shopfloor(%{station_id: station1_id}) |> page_size() == 2
      assert Defsol.list_reports_by_shopfloor(%{"station_id" => station1_id}) |> page_size() == 2
      assert Defsol.list_reports_by_shopfloor(%{station_id: station2_id}) |> page_size() == 3
      assert Defsol.list_reports_by_shopfloor(%{"station_id" => station2_id}) |> page_size() == 3

      assert Defsol.list_reports_by_shopfloor(%{part_id: part1_id}) |> page_size() == 2
      assert Defsol.list_reports_by_shopfloor(%{"part_id" => part1_id}) |> page_size() == 2
      assert Defsol.list_reports_by_shopfloor(%{part_id: part2_id}) |> page_size() == 3
      assert Defsol.list_reports_by_shopfloor(%{"part_id" => part2_id}) |> page_size() == 3

      assert Defsol.list_reports_by_shopfloor(%{part_id: part1_id, station_id: station1_id})
             |> page_size() == 2

      assert Defsol.list_reports_by_shopfloor(%{part_id: part1_id, station_id: station2_id})
             |> page_size() == 0
    end

    def extract_station_error(report) do
      report = report |> Hidria.Repo.preload(:error)
      {report.error.station_id, report.error.part_id}
    end
  end
end
