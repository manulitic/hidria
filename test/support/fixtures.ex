defmodule Hidria.Fixtures do
  import Hidria.Factory
  alias Hidria.Accounts.User

  def user(opts \\ []) do
    attrs = build(:user, opts) |> Map.from_struct()

    %User{}
    |> User.create_changeset(attrs)
    |> Hidria.Repo.insert!()
  end
end
