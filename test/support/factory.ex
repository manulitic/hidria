defmodule Hidria.Factory do
  use ExMachina.Ecto, repo: Hidria.Repo

  alias Hidria.Accounts.User
  alias Hidria.Shopfloor.Station
  alias Hidria.Shopfloor.Part
  alias Hidria.Defsol.Error
  alias Hidria.Defsol.Cause
  alias Hidria.Defsol.Solution
  alias Hidria.Defsol.Report
  alias Hidria.Defsol.Note

  def user_factory do
    %User{
      fullname: sequence("fullname"),
      username: sequence("username"),
      password: "password",
      password_confirmation: "password",
      role: Enum.random(Hidria.Accounts.roles())
    }
  end

  def station_factory do
    %Station{
      code: sequence("station"),
      description: sequence("station description")
    }
  end

  def part_factory do
    %Part{
      code: sequence("part"),
      description: sequence("part description")
    }
  end

  def error_factory do
    %Error{
      title: sequence("error"),
      description: sequence("description"),
      station_id: insert(:station).id,
      part_id: insert(:part).id
    }
  end

  def cause_factory do
    %Cause{
      title: sequence("title"),
      description: sequence("description"),
      detection: sequence("detection"),
      error_id: insert(:error).id
    }
  end

  def solution_factory do
    %Solution{
      title: sequence("title"),
      description: sequence("description"),
      steps: [%{title: sequence("step")}],
      cause_id: insert(:cause).id
    }
  end

  def report_factory do
    %Report{
      description: sequence("description")
    }
  end

  def note_factory do
    %Note{
      description: sequence("description")
    }
  end
end
