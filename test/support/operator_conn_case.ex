defmodule HidriaWeb.OperatorConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common datastructures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  alias Hidria.Fixtures
  import Hidria.Factory

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest
      import HidriaWeb.Router.Helpers

      # The default endpoint for testing
      @endpoint HidriaWeb.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Hidria.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Hidria.Repo, {:shared, self()})
    end

    operator = Fixtures.user(role: "operator")
    station = insert(:station)
    part = insert(:part)

    conn =
      Phoenix.ConnTest.build_conn()
      |> Plug.Session.call(session_init())
      |> Plug.Conn.assign(:current_user_id, operator.id)
      |> Plug.Conn.fetch_session()
      |> Plug.Conn.put_session(:current_station, station)
      |> Plug.Conn.put_session(:current_part, part)

    {:ok, conn: conn}
  end

  def session_init do
    Plug.Session.init(
      store: :cookie,
      key: "foobar",
      encryption_salt: "encrypted cookie salt",
      signing_salt: "signing salt",
      log: false,
      encrypt: false
    )
  end
end
